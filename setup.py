from setuptools import setup, find_packages
import os

version = '202412.14'
src_dir = 'src/main/python'
short_sha = os.getenv('CI_COMMIT_SHORT_SHA')
if short_sha:
    version += '+git.%s' % short_sha

setup(
    name='ipanema',
    version=version,
    python_requires='>=3',
    package_dir={'': src_dir},
    packages=find_packages(src_dir),
    package_data={'': ['*.sqlite', '*.json']},
    install_requires=['peewee>=3.16.0'],
    url='https://gitlab.com/jberkel/ipanema',
    author='Jan Berkel',
    author_email='jan@berkel.fr',
    description='Packaged language data',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ]
)
