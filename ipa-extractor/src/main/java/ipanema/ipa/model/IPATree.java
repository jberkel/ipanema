package ipanema.ipa.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class IPATree {
    private final Node root = new Node(0);
    private static class Node {
        public Optional<IPASound> sound = Optional.empty();
        public final int value;
        private final Map<Integer, Node> children = new HashMap<>();
        private Node(int value) {
            this.value = value;
        }

        private Node insert(int number) {
            Node node = children.get(number);
            if (node == null) {
                node = new Node(number);
                children.put(number, node);
            }
            return node;
        }

        public Node insert(int... numbers) {
            Node start = this;
            for (int number : numbers) {
                start = start.insert(number);
            }
            return start;
        }
        public Node get(int codePoint) {
            return children.get(codePoint);
        }
    }

    public IPATree(Collection<IPASound> sounds) {
        for (IPASound sound : sounds) {
            root.insert(sound.codepoints).sound = Optional.of(sound);
        }
    }

    public static IPATree fromJSON(InputStream input) {
        requireNonNull(input);

        try {
            TypeReference<List<IPASound>> ref = new TypeReference<List<IPASound>>() { };
            final ObjectMapper mapper = new ObjectMapper();
            List<IPASound> parsed =  mapper.reader().readValue(mapper.getFactory().createParser(input), ref);
            return new IPATree(parsed);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static class Match {
        public final Optional<IPASound> sound;
        public final String consumed, remaining;

        public Match(Optional<IPASound> sound, String consumed, String remaining) {
            this.sound = sound;
            this.consumed = consumed;
            this.remaining = remaining;
        }
    }

    public Match find(String string) {
        Node current = root;
        int i = 0;
        while (i < string.length() ) {
            Node next = current.get(string.codePointAt(i));
            if (next != null) {
                current = next;
                i++;
            } else {
                break;
            }
        }
        if (current != root) {
            i--;
        }
        final String consumed = string.substring(0, i+1);
        final String remaining = i >= string.length() - 1 ? null : string.substring(i+1);
        return new Match(current.sound, consumed, remaining);
    }
}
