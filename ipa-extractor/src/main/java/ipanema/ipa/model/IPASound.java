package ipanema.ipa.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class IPASound {
    public final String name;
    public final String link;
    public final int[] codepoints;
    public final int[] ipanumbers;
    public final String representation;

    @JsonCreator
    public IPASound(@JsonProperty("name") String name,
                    @JsonProperty("link") String link,
                    @JsonProperty("codepoints") int[] codepoints,
                    @JsonProperty("ipanumbers") int[] ipanumbers) {
        this.name = name;
        this.link = link;
        this.codepoints = codepoints;
        this.ipanumbers = ipanumbers;
        this.representation = new String(codepoints, 0, codepoints.length);
    }

    @Override
    public String toString() {
        return "IPASound{" +
                "name='" + name + '\'' +
                ", representation='" + representation + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IPASound ipaSound = (IPASound) o;
        return Arrays.equals(codepoints, ipaSound.codepoints);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(codepoints);
    }
}
