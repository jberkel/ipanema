package ipanema.ipa;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import ipanema.ipa.extract.Parser;
import ipanema.ipa.model.IPASound;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class ProcessJSON {
    private static final Logger LOG = Logger.getLogger(ProcessJSON.class.getName());
    private static final boolean FETCH_AUDIO = false;

    private Parser parser;

    private ProcessJSON() {
        this.parser = new Parser();
    }

    public static void main(String[] args) throws IOException {
        try (FileReader reader = new FileReader(args[0])) {
            new ProcessJSON().process(reader, FETCH_AUDIO);
        }
    }

    private void process(Reader reader, boolean fetchAudio) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        final JsonNode jsonNode = mapper.reader().readTree(reader);
        final Stream<String> links = jsonNode.findValuesAsText("link").stream().map(l -> l.replace("w:", ""));

        serialize(mapper, process(links, fetchAudio));
    }

    private Set<IPASound> process(Stream<String> links, boolean fetchAudio) {
        return links.parallel().map(articleName -> {
            try {
                return parser.parse(articleName, fetchAudio);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).flatMap(info -> info.getIPAInfo().stream()
                .filter(box -> box.decimal[0] != 0)
                .map(box -> {
                    String name = box.above != null ? box.above : info.getTitle();
                    return new IPASound(name, info.getLink(), box.codepoints(), box.numbers());
                })).reduce(Collections.synchronizedSet(new HashSet<>()), (set, sound) -> {
            set.add(sound);
            return set;
        }, (a, b) -> {
            Set<IPASound> combined = Collections.synchronizedSet(new HashSet<>(a));
            combined.addAll(b);
            return combined;
        });
    }

    private void serialize(ObjectMapper mapper, Set<IPASound> data) throws IOException {
        final File json = new File("data/ipa.json");
        if (json.getParentFile().exists()) {
            json.getParentFile().mkdirs();
        }
        try (PrintStream ps = new PrintStream(new FileOutputStream(json))) {
            final ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();

            List<IPASound> list = new ArrayList<>(data);
            list.sort((o1, o2) -> o1.link.compareTo(o2.link));
            writer.writeValue(ps, list);
        }
    }
}
