package ipanema.ipa.extract;

import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.engine.PageTitle;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class IPAInfo {
    private final PageId pageId;
    private final PageTitle pageTitle;
    private final List<IPAInfoBox> boxes;
    private final String articleText;

    // fields not directly used by parser
    public File articleFile;
    public Optional<File> soundFile;

    public IPAInfo(PageId pageId, PageTitle title, List<IPAInfoBox> boxes, String text) {
        this.pageId = pageId;
        this.boxes = boxes;
        this.articleText = text;
        this.pageTitle = title;
    }

    public File getSoundFile() {
        return boxes.stream()
                .filter(i -> i.soundfile != null)
                .findFirst()
                .map(i -> new File(i.soundfile))
                .orElse(getTargetSoundFile());
    }

    public File getTargetSoundFile() {
        return new File(getPageTitle().getNormalizedFullTitle() + Parser.OGG);
    }

    public PageTitle getPageTitle() {
        return pageTitle;
    }

    @Override
    public String toString() {
        return "IPAInfo{" +
                "pageId=" + pageId +
                ", boxes=" + boxes +
                ", articleText='" + articleText + '\'' +
                ", pageTitle='" + pageTitle + '\'' +
                '}';
    }

    public String getArticleText() {
        return articleText;
    }

    public String getTitle() {
        return pageId.getTitle().getDenormalizedFullTitle();
    }

    public String getLink() {
        return pageId.getTitle().getNormalizedFullTitle();
    }

    public List<IPAInfoBox> getIPAInfo() {
        return boxes;
    }
}
