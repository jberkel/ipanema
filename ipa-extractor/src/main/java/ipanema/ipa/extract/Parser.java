package ipanema.ipa.extract;

import ipanema.ipa.extract.visitor.HashMapTemplateCache;
import ipanema.ipa.extract.visitor.IPAInfoBoxCollector;
import ipanema.ipa.extract.visitor.IPALink;
import ipanema.ipa.extract.visitor.RedisTemplateCache;
import ipanema.ipa.extract.visitor.TextConverter;
import net.sourceforge.jwbf.core.actions.HttpActionClient;
import net.sourceforge.jwbf.core.actions.util.ProcessException;
import net.sourceforge.jwbf.mediawiki.actions.queries.ImageInfo;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;
import org.sweble.wikitext.engine.EngineException;
import org.sweble.wikitext.engine.ExpansionDebugHooks;
import org.sweble.wikitext.engine.ExpansionVisitor;
import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.engine.WtEngine;
import org.sweble.wikitext.engine.WtEngineImpl;
import org.sweble.wikitext.engine.config.WikiConfigImpl;
import org.sweble.wikitext.engine.nodes.EngProcessedPage;
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp;
import org.sweble.wikitext.parser.nodes.WtNode;
import org.sweble.wikitext.parser.nodes.WtRedirect;
import org.sweble.wikitext.parser.parser.LinkTargetException;
import xtc.parser.ParseException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import static java.util.Arrays.asList;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

public class Parser {
    private static final Logger LOG = Logger.getLogger(Parser.class.getName());
    private static final String EN_WIKIPEDIA = "https://en.wikipedia.org/w/";

    private static final File DATA_DIR = new File("data");
    private static final File MEDIA_DIR = new File(DATA_DIR, "media");
    private static final File ARTICLES_DIR = new File(DATA_DIR, "articles");

    private static final List<String> SKIP_SECTIONS = asList(
            "See also", "References",
            "Notes", "Bibliography", "External links");

    static final String OGG = ".ogg";
    private static final String OGA = ".oga";

    private static final boolean FETCH_AUDIO = true;

    private static final String USER_AGENT_NAME = "ipanema";
    private static final String USER_AGENT_VERSION = "1.0";
    private static final String USER_AGENT = USER_AGENT_NAME + " " + USER_AGENT_VERSION;
    private static final String USER_AGENT_COMMENT = "https://gitlab.com/jberkel/ipanema";
    private static final int TEXT_WRAP_COLUMNS = 100;

    private final WtEngine engine;
    private final MediaWikiBot bot;
    private final TemplateCache templateCache;

    public Parser() {
        WikiConfigImpl wikiConfig = DefaultConfigEnWp.generate();
        templateCache = new HashMapTemplateCache(Optional.of(new RedisTemplateCache()));
        engine = new WtEngineImpl(wikiConfig);
        HttpActionClient client = HttpActionClient.builder()
            .withUrl(EN_WIKIPEDIA)
            .withUserAgent(USER_AGENT_NAME, USER_AGENT_VERSION, USER_AGENT_COMMENT)
            .build();
        bot = new MediaWikiBot(client);
    }

    public static void main(String[] args) throws Exception {
        new Parser().parse(args[0], FETCH_AUDIO);
    }

    public IPAInfo parse(String articleName, boolean fetchAudio) throws IOException, ParseException, LinkTargetException, EngineException {
        final PageTitle pageTitle = PageTitle.make(engine.getWikiConfig(), articleName);
        final IPAInfo info = parse(
                pageTitle,
                String.format("#REDIRECT [[%s]]", pageTitle.getDenormalizedTitle())
                );

        if (fetchAudio) {
            info.soundFile = downloadSoundFile(info.getSoundFile(), info.getTargetSoundFile());
        }
        if (info.getArticleText() != null && !info.getArticleText().trim().isEmpty()) {
            info.articleFile = writeFile(info.getArticleText(), info.getPageTitle());
        } else {
            LOG.warning("empty content for "+info.getPageTitle());
        }
        return info;
    }

    private File writeFile(String text, PageTitle pageTitle) throws IOException {
        File output = new File(ARTICLES_DIR, pageTitle.getTitle() + ".md");
        mkdirs(output);
        try (PrintStream ps = new PrintStream(new FileOutputStream(output))) {
            ps.print(text);
        }
        return output;
    }

    private Optional<File> downloadSoundFile(File soundFile, File target) throws IOException {
        final File destFile = new File(MEDIA_DIR, target.getName());
        if (!destFile.exists()) {
            return getMediaUrl(soundFile.getName()).map(url -> {
                try {
                    LOG.log(INFO, "downloading sound file " + url);
                    return downloadURL(url, destFile);
                } catch (IOException e) {
                    LOG.log(WARNING, "error downloading file", e);
                    throw new RuntimeException(e);
                }
            });
        } else {
            return Optional.of(destFile);
        }
    }

    private Optional<URL> getMediaUrl(String fileName) {
        ImageInfo fileInfo = new ImageInfo(bot, fileName);
        try {
            return Optional.of(fileInfo.getUrl());
        } catch (ProcessException e) {
            // hack
            if (fileName.endsWith(OGG)) {
                return getMediaUrl(fileName.replace(OGG, OGA));
            } else {
               return Optional.empty();
            }
         }
    }

    private File downloadURL(URL url, File file) throws IOException {
        mkdirs(file);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("User-Agent", USER_AGENT);
        try (InputStream is = conn.getInputStream()) {
            try (FileOutputStream fos = new FileOutputStream(file)) {
                byte[] buffer = new byte[8192];
                int n;
                while ((n = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, n);
                }
            }
        }
        return file;
    }

    private static void mkdirs(File file) {
        if (!file.getParentFile().exists()) {
            final boolean mkdirs = file.getParentFile().mkdirs();
            assert mkdirs;
        }
    }

    IPAInfo parse(String title, String text) throws EngineException, LinkTargetException {
        return parse(PageTitle.make(engine.getWikiConfig(), title), text);
    }

    private IPAInfo parse(PageTitle pageTitle, String text) throws EngineException, LinkTargetException {
        return parse(new PageId(pageTitle, -1), text);
    }

    private IPAInfo parse(PageId pageId, String text) throws EngineException {
        final String[] redirectedTo = new String[1];
        engine.setDebugHooks(new ExpansionDebugHooks() {
            @Override
            public WtNode beforeResolveRedirect(ExpansionVisitor expansionVisitor, WtRedirect n, String target) {
                if (expansionVisitor.getExpFrame().getTitle().equals(pageId.getTitle())) {
                    final String targetLink = n.getTarget().getAsString();
                    if (!expansionVisitor.getExpFrame().getTitle().getDenormalizedFullTitle().equals(targetLink)) {
                        LOG.warning("redirect " + pageId.getTitle() + " ==> " + targetLink);
                    }
                    redirectedTo[0] = targetLink;
                }
                return super.beforeResolveRedirect(expansionVisitor, n, target);
            }
        });

        final EngProcessedPage processedPage = engine.postprocess(pageId, text, new FetchingCallback(
                bot,
                engine.getWikiConfig(),
                templateCache,
                LOG,
                title -> !IPAInfoBoxTemplateDataExtractor.isIPAInfoBox(title) && !IPALink.isIPALink(title)
            ));

        TextConverter converter = getTextConverter();
        final String pageText =  converter.go(processedPage.getPage()).toString();
        if (converter.getCategories().contains("Disambiguation pages")) {
            throw new EngineException(pageId.getTitle(), "Ambiguous page", null);
        }

        List<IPAInfoBox> boxes = new IPAInfoBoxCollector<List<IPAInfoBox>>(pageId).go(processedPage);
        final PageTitle title;
        if (redirectedTo[0] != null) {
            try {
                title = PageTitle.make(engine.getWikiConfig(), redirectedTo[0]);
            } catch (LinkTargetException e) {
                throw new RuntimeException(e);
            }
        } else {
            title = pageId.getTitle();
        }
        return new IPAInfo(pageId, title, boxes, pageText);
    }

    private TextConverter getTextConverter() {
        return new TextConverter(engine.getWikiConfig(),
            TEXT_WRAP_COLUMNS,
            s -> !SKIP_SECTIONS.contains(s));
    }
}
