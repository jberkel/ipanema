package ipanema.ipa.extract;

import org.sweble.wikitext.engine.FullPage;
import org.sweble.wikitext.engine.PageTitle;
import redis.clients.jedis.exceptions.JedisException;

import java.util.Optional;

public interface TemplateCache {
    class CacheException extends RuntimeException {
        private static final long serialVersionUID = 3212820901577522376L;

        public CacheException(JedisException e) {
            super(e);
        }
    }

    /**
     * @param title the title of the page to get
     * @return the full page, or empty
     * @throws CacheException thrown in case of configuration problems
     */
    Optional<FullPage> get(PageTitle title) throws CacheException;

    /**
     * @param page the page to cache
     * @throws CacheException thrown in case of configuration problems
     */
    void store(FullPage page) throws CacheException;
}
