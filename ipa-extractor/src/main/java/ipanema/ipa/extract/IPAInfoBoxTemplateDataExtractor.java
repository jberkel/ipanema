package ipanema.ipa.extract;

import ipanema.ipa.extract.visitor.WtTemplateArgsHelper;
import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.parser.nodes.WtNode;
import org.sweble.wikitext.parser.nodes.WtTemplate;
import org.sweble.wikitext.parser.nodes.WtTemplateArgument;

import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;

import static java.util.logging.Level.WARNING;


/**
<pre>
{{infobox IPA
| above        =
| ipa symbol   =
| ipa symbol2  =
| ipa symbol3  =
| ipa symbol4  =

| imagefile    =
| imagecaption =
| imagesize    =

| ipa number   =
| ipa link     =
| decimal1     =
| decimal2     =
| decimal3     =
| decimal4     =
| note1 label  =
| note1        =

| unicode      =
| x-sampa      =
| kirshenbaum  =
| braille      =
| braille2     =
| braille3     =
| braille4     =
| note2 label  =
| note2        =

| soundfile    =
}}
</pre>
 @see <a href="https://en.wikipedia.org/wiki/Template:Infobox_IPA">Template:Infobox_IPA</a>
 */
public class IPAInfoBoxTemplateDataExtractor implements TemplateDataExtractor<IPAInfoBox> {
    private static final Logger LOG = Logger.getLogger(IPAInfoBoxTemplateDataExtractor.class.getSimpleName());
    public static final String INFOBOX_IPA = "Infobox IPA";

    public static boolean isIPAInfoBox(PageTitle title) {
        return title.getDenormalizedTitle().equalsIgnoreCase(INFOBOX_IPA);
    }

    @Override
    public IPAInfoBox extract(WtTemplate template, PageId pageId) {
        final Function<String, Optional<Integer>> intParser = createIntParser(template, pageId);
        final IPAInfoBox infoBox = new IPAInfoBox();
        for (WtNode argument : template.getArgs()) {
            WtTemplateArgument arg = (WtTemplateArgument) argument;
            if (!arg.getName().isResolved()) {
                LOG.log(WARNING, "template arg " + arg + " is not resolved");
                continue;
            }
            final String name = arg.getName().getAsString().trim();
            String value = WtTemplateArgsHelper.extractText(arg);

            if (value == null) {
                LOG.log(WARNING, "no value for " + name);
                continue;
            }
            switch (name) {
                case "ipa number": {
                    infoBox.numberRaw = value;
                    if (value.contains("&ndash;")) {
                        // Tone_letter, ignore for now
                        break;
                    }
                    value = value.replace("+", " ")
                                 .replace(",", " ")
                                 .replace("<br/>", " ");
                    if (value.contains(" ")) {
                        final String[] split = value.split("\\s+", 4);
                        for (int i=0; i<split.length; i++) {
                            try {
                                infoBox.number[i] = Integer.parseInt(split[i].trim());
                            } catch (NumberFormatException e) {
                                LOG.warning("error converting number "+value);
                            }
                        }
                    } else {
                        infoBox.number[0] = intParser.apply(value).orElse(0);
                    }
                    break;
                }

                case "decimal":
                case "decimal1": infoBox.decimal[0] = intParser.apply(value).orElse(0); break;
                case "decimal2": infoBox.decimal[1] = intParser.apply(value).orElse(0); break;
                case "decimal3": infoBox.decimal[2] = intParser.apply(value).orElse(0); break;
                case "decimal4": infoBox.decimal[3] = intParser.apply(value).orElse(0); break;

                case "above": infoBox.above = value; break;
                case "xsampa": infoBox.xsampa = value; break;
                case "kirshenbaum": infoBox.kirshenbaum = value; break;
                case "imagefile": infoBox.imagefile = value;  break;
                case "braille": infoBox.braille = value;  break;
                case "braille2": infoBox.braille2 = value;  break;
                case "soundfile": infoBox.soundfile = value;  break;
                case "ipasymbol": infoBox.ipasymbol = value;  break;
            }
        }
        return infoBox;
    }


    private Function<String, Optional<Integer>> createIntParser(WtTemplate template, PageId pageId) {
        return value -> {
            if (value == null || value.isEmpty()) {
                LOG.severe("value is empty in "+template + ", page "+pageId.getTitle());
                return Optional.empty();
            }

            try {
                return Optional.of(Integer.parseInt(value));
            } catch (NumberFormatException e) {
                LOG.severe("error extracting IPA number '"+value+"' in "+template+", page "+pageId.getTitle());
                return Optional.empty();
            }
        };
    }
}
