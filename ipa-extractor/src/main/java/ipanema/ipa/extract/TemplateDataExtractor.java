package ipanema.ipa.extract;

import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.parser.nodes.WtTemplate;

public interface TemplateDataExtractor<T> {
    T extract(WtTemplate template, PageId pageId);
}
