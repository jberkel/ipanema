package ipanema.ipa.extract;

import java.util.Arrays;

import static java.util.Arrays.asList;

public class IPAInfoBox {
    public String numberRaw;
    public final int[] number = new int[4];
    /**
     * Decimals are decimal character references, also known as "Entity", to define the symbol in HTML.
     */
    public final int[] decimal = new int[4];

    public String xsampa;
    public String kirshenbaum;
    public String imagefile;

    public String above;

    public String braille;
    public String braille2;
    public String soundfile;
    public String ipasymbol;

    @Override
    public String toString() {
        return "IPAInfoBox{" +
                ", number=" + asList(number) +
                ", decimal=" + asList(decimal) +
                ", above='" + above + '\'' +
                ", xsampa='" + xsampa + '\'' +
                ", kirshenbaum='" + kirshenbaum + '\'' +
                ", imagefile='" + imagefile + '\'' +
                ", braille='" + braille + '\'' +
                ", braille2='" + braille2 + '\'' +
                ", soundFile='" + soundfile + '\'' +
                '}';
    }

    public int[] codepoints() {
        return Arrays.stream(decimal).filter(d -> d > 0).toArray();
    }

    public int[] numbers() {
        return Arrays.stream(number).filter(d -> d > 0).toArray();
    }
}
