package ipanema.ipa.extract;

import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;
import org.sweble.wikitext.engine.ExpansionCallback;
import org.sweble.wikitext.engine.ExpansionFrame;
import org.sweble.wikitext.engine.FullPage;
import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.engine.config.WikiConfig;
import org.sweble.wikitext.parser.parser.LinkTargetException;

import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;

import static java.util.logging.Level.WARNING;

class FetchingCallback implements ExpansionCallback {
    private final MediaWikiBot bot;
    private final WikiConfig wikiconfig;
    private final Logger logger;
    private final TemplateCache templateCache;
    private Function<PageTitle, Boolean> filter;

    FetchingCallback(MediaWikiBot bot,
                     WikiConfig wikiconfig,
                     TemplateCache templateCache,
                     Logger logger,
                     Function<PageTitle, Boolean> filter) {
        this.bot = bot;
        this.wikiconfig = wikiconfig;
        this.templateCache = templateCache;
        this.logger = logger;
        this.filter = filter;
    }

    @Override
    public FullPage retrieveWikitext(ExpansionFrame expansionFrame, PageTitle pageTitle) {
        if (!shouldExpand(pageTitle)) {
            return null;
        }

        final Optional<FullPage> cachedPage;
        try {
            cachedPage = templateCache.get(pageTitle);
        } catch (TemplateCache.CacheException e) {
            logger.log(WARNING, "error fetching cached page", e);
            throw e;
        }

        if (cachedPage.isPresent()) {
            return cachedPage.get();
        }

        logger.info("retrieveWikitext(" + pageTitle + ")");

        final Article article = bot.getArticle(pageTitle.getDenormalizedFullTitle());
        final PageTitle title;
        try {
            title = PageTitle.make(wikiconfig, article.getTitle());
        } catch (LinkTargetException e) {
            throw new RuntimeException(e);
        }
        long revision;
        try {
            revision = Long.parseLong(article.getRevisionId());
        } catch (NumberFormatException e) {
            revision = -1;
        }
        final PageId pageId = new PageId(title, revision);

        FullPage page = new FullPage(pageId, article.getText());
        templateCache.store(page);

        return page;
    }

    private boolean shouldExpand(PageTitle title) {
        return !"Template:".equals(title.getDenormalizedFullTitle())
                && !title.getNamespace().isMediaNs()
                && (filter == null || filter.apply(title));
    }

    @Override
    public String fileUrl(PageTitle pageTitle, int width, int height) {
        return null;
    }
}
