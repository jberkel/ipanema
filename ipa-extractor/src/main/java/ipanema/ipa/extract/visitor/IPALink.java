package ipanema.ipa.extract.visitor;

import org.sweble.wikitext.engine.PageTitle;

public class IPALink {
    static final String IPALINK = "IPAlink";
    static final String IPALINK2 = "IPA link";

    public static boolean isIPALink(PageTitle title) {
        return title.getDenormalizedTitle().equalsIgnoreCase(IPALINK) ||
                title.getDenormalizedTitle().equalsIgnoreCase(IPALINK2);
    }
}
