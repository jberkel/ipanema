package ipanema.ipa.extract.visitor;

import de.fau.cs.osr.ptk.common.AstVisitor;
import de.fau.cs.osr.utils.StringTools;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.engine.config.WikiConfig;
import org.sweble.wikitext.engine.nodes.EngNowiki;
import org.sweble.wikitext.engine.nodes.EngPage;
import org.sweble.wikitext.parser.nodes.WtBold;
import org.sweble.wikitext.parser.nodes.WtExternalLink;
import org.sweble.wikitext.parser.nodes.WtHorizontalRule;
import org.sweble.wikitext.parser.nodes.WtIllegalCodePoint;
import org.sweble.wikitext.parser.nodes.WtImEndTag;
import org.sweble.wikitext.parser.nodes.WtImStartTag;
import org.sweble.wikitext.parser.nodes.WtImageLink;
import org.sweble.wikitext.parser.nodes.WtInternalLink;
import org.sweble.wikitext.parser.nodes.WtItalics;
import org.sweble.wikitext.parser.nodes.WtListItem;
import org.sweble.wikitext.parser.nodes.WtNewline;
import org.sweble.wikitext.parser.nodes.WtNode;
import org.sweble.wikitext.parser.nodes.WtNodeList;
import org.sweble.wikitext.parser.nodes.WtOrderedList;
import org.sweble.wikitext.parser.nodes.WtPageSwitch;
import org.sweble.wikitext.parser.nodes.WtParagraph;
import org.sweble.wikitext.parser.nodes.WtSection;
import org.sweble.wikitext.parser.nodes.WtTable;
import org.sweble.wikitext.parser.nodes.WtTableCaption;
import org.sweble.wikitext.parser.nodes.WtTableCell;
import org.sweble.wikitext.parser.nodes.WtTableHeader;
import org.sweble.wikitext.parser.nodes.WtTableImplicitTableBody;
import org.sweble.wikitext.parser.nodes.WtTableRow;
import org.sweble.wikitext.parser.nodes.WtTagExtension;
import org.sweble.wikitext.parser.nodes.WtTemplate;
import org.sweble.wikitext.parser.nodes.WtTemplateArgument;
import org.sweble.wikitext.parser.nodes.WtTemplateParameter;
import org.sweble.wikitext.parser.nodes.WtText;
import org.sweble.wikitext.parser.nodes.WtTicks;
import org.sweble.wikitext.parser.nodes.WtUnorderedList;
import org.sweble.wikitext.parser.nodes.WtUrl;
import org.sweble.wikitext.parser.nodes.WtWhitespace;
import org.sweble.wikitext.parser.nodes.WtXmlAttribute;
import org.sweble.wikitext.parser.nodes.WtXmlCharRef;
import org.sweble.wikitext.parser.nodes.WtXmlComment;
import org.sweble.wikitext.parser.nodes.WtXmlElement;
import org.sweble.wikitext.parser.nodes.WtXmlEmptyTag;
import org.sweble.wikitext.parser.nodes.WtXmlEndTag;
import org.sweble.wikitext.parser.nodes.WtXmlEntityRef;
import org.sweble.wikitext.parser.nodes.WtXmlStartTag;
import org.sweble.wikitext.parser.parser.LinkTargetException;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static ipanema.ipa.extract.visitor.WtTemplateArgsHelper.extractText;

@SuppressWarnings("unused")
public class TextConverter extends AstVisitor<WtNode> {
    private static final Pattern ws = Pattern.compile("\\s+");
    private final WikiConfig config;
    private final int wrapCol;
    private StringBuilder sb;
    private StringBuilder line;
    private int extLinkNum;
    private boolean pastBod;
    private int needNewlines;
    private boolean needSpace;
    private boolean noWrap;
    private LinkedList<Integer> sections;
    private final Predicate<String> includeSection;
    private final Set<String> categories = new HashSet<>();

    public TextConverter(WikiConfig config, int wrapCol,
                         Predicate<String> includeSection) {
        this.config = config;
        this.wrapCol = wrapCol;
        this.includeSection = includeSection;
    }

    public Set<String> getCategories() {
        return new HashSet<>(categories);
    }

    @Override
    protected WtNode before(WtNode node) {
        // This method is called by go() before visitation starts
        sb = new StringBuilder();
        line = new StringBuilder();
        extLinkNum = 1;
        pastBod = false;
        needNewlines = 0;
        needSpace = false;
        noWrap = false;
        sections = new LinkedList<>();
        return super.before(node);
    }

    @Override
    protected Object after(WtNode node, Object result) {
        finishLine();
        return sb.toString();
    }

    public void visit(WtNode n) {
        // Fallback for all nodes that are not explicitly handled below
        if (true) {
            throw new RuntimeException("unhandled node type "+n.getClass().getSimpleName());
        }
        write("<");
        write(n.getNodeName());
        write(" />");
    }

    public void visit(EngNowiki n) {
        iterate(n);
    }

    public void visit(WtNodeList n) {
        iterate(n);
    }

    public void visit(WtUnorderedList e) {
        iterate(e);
    }

    public void visit(WtOrderedList e) {
        iterate(e);
    }

    public void visit(WtListItem item) {
        newline(1);
        write("* ");
        iterate(item);
    }

    public void visit(EngPage p) {
        iterate(p);
    }

    public void visit(WtText text) {
        write(text.getContent());
    }

    public void visit(WtWhitespace w) {
        write(" ");
    }

    public void visit(WtBold b) {
        write("**");
        write(renderToString(() -> iterate(b)));
        write("**");
    }

    public void visit(WtItalics i) {
        write("_");
        iterate(i);
        write("_");
    }

    public void visit(WtImStartTag b) {
    }

    public void visit(WtImEndTag b) {
    }

    public void visit(WtTable b) {
        iterate(b);
    }

    public void visit(WtTableCaption b) {
    }

    public void visit(WtTableImplicitTableBody b) {
        iterate(b);
    }

    public void visit(WtTableRow b) {
        iterate(b);
    }

    public void visit(WtTableHeader b) {
    }

    public void visit(WtTableCell b) {
    }

    public void visit(WtTicks b) {
    }

    public void visit(WtXmlStartTag b) {
    }

    public void visit(WtXmlEmptyTag b) {
    }

    public void visit(WtXmlAttribute b) {
    }

    public void visit(WtXmlEndTag b) {
    }

    public void visit(WtNewline newline) {
        writeNewlines(1);
    }

    public void visit(WtXmlCharRef cr) {
        write(Character.toChars(cr.getCodePoint()));
    }

    public void visit(WtXmlEntityRef er) {
        String ch = er.getResolved();
        if (ch == null) {
            write('&');
            write(er.getName());
            write(';');
        } else {
            write(ch);
        }
    }

    public void visit(WtUrl wtUrl) {
        if (!wtUrl.getProtocol().isEmpty()) {
            write(wtUrl.getProtocol());
            write(':');
        }
        write(wtUrl.getPath());
    }

    public void visit(WtExternalLink link) {
        write('[');
        write(extLinkNum++);
        write(']');
    }

    public void visit(WtInternalLink link) {
        try {
            if (link.getTarget().isResolved()) {
                PageTitle page = PageTitle.make(config, link.getTarget().getAsString());
                if (page.getNamespace().equals(config.getNamespace("Category"))) {
                    categories.add(page.getDenormalizedTitle());
                    return;
                }
            }
        } catch (LinkTargetException ignored) {
        }

        write(link.getPrefix());
        if (!link.hasTitle()) {
            iterate(link.getTarget());
        } else {
            iterate(link.getTitle());
        }
        write(link.getPostfix());
    }

    public void visit(WtSection s) {
        finishLine();
        StringBuilder saveSb = sb;
        boolean saveNoWrap = noWrap;

        sb = new StringBuilder();
        noWrap = true;

        iterate(s.getHeading());
        finishLine();
        String title = sb.toString().trim();
        sb = saveSb;

        if (includeSection != null && s.getLevel() == 2 && !includeSection.test(title)) {
            return;
        }

        if (s.getLevel() >= 1) {
            while (sections.size() > s.getLevel())
                sections.removeLast();
            while (sections.size() < s.getLevel())
                sections.add(1);

            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < sections.size(); ++i) {
                if (i < 1)
                    continue;

                sb2.append(sections.get(i));
                sb2.append('.');
            }

            if (sb2.length() > 0)
                sb2.append(' ');
            sb2.append(title);
            title = sb2.toString();
        }

        newline(2);
        write(StringTools.strrep("#", s.getLevel()));
        write(" ");
        write(title);
        newline(2);

        noWrap = saveNoWrap;

        iterate(s.getBody());

        while (sections.size() > s.getLevel())
            sections.removeLast();
        sections.add(sections.removeLast() + 1);
    }

    public void visit(WtParagraph p) {
        iterate(p);
        newline(2);
    }

    public void visit(WtHorizontalRule hr) {
        newline(1);
        write(StringTools.strrep('-', wrapCol));
        newline(2);
    }

    public void visit(WtXmlElement e) {
        if (e.getName().equalsIgnoreCase("br")) {
            newline(1);
        } else {
            iterate(e.getBody());
        }
    }

    public void visit(WtImageLink n) {
    }

    public void visit(WtIllegalCodePoint n) {
    }

    public void visit(WtXmlComment n) {
    }

    public void visit(WtTemplate n) {
        switch (n.getName().getAsString().trim()) {
            case IPALink.IPALINK:
            case IPALink.IPALINK2:
                final String target = extractText(n.getArgs(), 0);
                if (target != null) {
                    write(target);
                }
                break;
            default:
                break;
        }
    }

    public void visit(WtTemplateArgument n) {
    }

    public void visit(WtTemplateParameter n) {
    }

    public void visit(WtTagExtension n) {
    }

    public void visit(WtPageSwitch n) {
    }

    private void newline(int num) {
        if (pastBod) {
            if (num > needNewlines)
                needNewlines = num;
        }
    }

    private void wantSpace() {
        if (pastBod)
            needSpace = true;
    }

    private void finishLine() {
        sb.append(line.toString());
        line.setLength(0);
    }

    private void writeNewlines(int num) {
        finishLine();
        sb.append(StringTools.strrep('\n', num));
        needNewlines = 0;
        needSpace = false;
    }

    private void writeWord(String s) {
        int length = s.length();
        if (length == 0)
            return;

        if (!noWrap && needNewlines <= 0) {
            if (needSpace)
                length += 1;

            if (line.length() + length >= wrapCol && line.length() > 0)
                writeNewlines(1);
        }

        if (needSpace && needNewlines <= 0)
            line.append(' ');

        if (needNewlines > 0)
            writeNewlines(needNewlines);

        needSpace = false;
        pastBod = true;
        line.append(s);
    }

    private void write(String s) {
        if (s.isEmpty())
            return;

        if (Character.isSpaceChar(s.charAt(0)))
            wantSpace();

        String[] words = ws.split(s);
        for (int i = 0; i < words.length; ) {
            writeWord(words[i]);
            if (++i < words.length)
                wantSpace();
        }

        if (Character.isSpaceChar(s.charAt(s.length() - 1)))
            wantSpace();
    }

    private void write(char[] cs) {
        write(String.valueOf(cs));
    }
    private void write(char ch) {
        writeWord(String.valueOf(ch));
    }
    private void write(int num) {
        writeWord(String.valueOf(num));
    }

    private String renderToString(Runnable r) {
        StringBuilder saveSb = sb;
        boolean saveNoWrap = noWrap;

        sb = new StringBuilder();
        noWrap = true;

        r.run();
        String content = sb.toString().trim();

        sb = saveSb;
        noWrap = saveNoWrap;

        return content;
    }
}
