package ipanema.ipa.extract.visitor;

import ipanema.ipa.extract.IPAInfoBox;
import ipanema.ipa.extract.IPAInfoBoxTemplateDataExtractor;
import ipanema.ipa.extract.TemplateDataExtractor;
import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.parser.nodes.WtNode;
import org.sweble.wikitext.parser.nodes.WtTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static ipanema.ipa.extract.IPAInfoBoxTemplateDataExtractor.INFOBOX_IPA;

public class IPAInfoBoxCollector<T> extends PageVisitor {
    private List<IPAInfoBox> ipaInfo;
    private final PageId pageId;

    public IPAInfoBoxCollector(PageId pageId) {
        super();
        this.pageId = pageId;
        ipaInfo = new ArrayList<>();
    }

    private static Map<String, TemplateDataExtractor<IPAInfoBox>> TEMPLATES = new HashMap<>();


    static {
        TEMPLATES.put(INFOBOX_IPA.toLowerCase(Locale.ENGLISH), new IPAInfoBoxTemplateDataExtractor());
    }

    private List<IPAInfoBox> getIpaInfo() {
        return ipaInfo;
    }

    @Override @SuppressWarnings("unchecked")
    public T go(WtNode node) {
        return (T) super.go(node);
    }

    @Override
    protected Object after(WtNode node, Object result) {
        return getIpaInfo();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void visit(WtTemplate template) {
        if (!template.getName().isResolved()) {
            return;
        }
        final String templateName = template.getName().getAsString().trim();

        final TemplateDataExtractor<IPAInfoBox> td = TEMPLATES.get(templateName.toLowerCase(Locale.ENGLISH));
        if (td != null) {
            ipaInfo.add(td.extract(template, pageId));
        }
    }
}
