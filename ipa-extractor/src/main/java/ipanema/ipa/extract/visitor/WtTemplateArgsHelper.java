package ipanema.ipa.extract.visitor;

import org.sweble.wikitext.parser.nodes.WtTemplateArgument;
import org.sweble.wikitext.parser.nodes.WtTemplateArguments;
import org.sweble.wikitext.parser.nodes.WtText;

public class WtTemplateArgsHelper {
    public static String extractText(WtTemplateArguments args, int param) {
        if (param >= 0 && param < args.size()) {
            return extractText((WtTemplateArgument) args.get(param));
        } else {
            return null;
        }
    }

    public static String extractText(WtTemplateArgument arg) {
        if (!arg.getValue().isEmpty() && arg.getValue().get(0) instanceof WtText) {
            return ((WtText) arg.getValue().get(0)).getContent().trim();
        } else {
            return null;
        }
    }
}
