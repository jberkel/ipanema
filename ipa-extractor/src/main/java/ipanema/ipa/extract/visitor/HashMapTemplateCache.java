package ipanema.ipa.extract.visitor;

import ipanema.ipa.extract.TemplateCache;
import org.sweble.wikitext.engine.FullPage;
import org.sweble.wikitext.engine.PageTitle;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class HashMapTemplateCache implements TemplateCache {
    private Map<PageTitle, FullPage> cache = new HashMap<>();
    private final Optional<TemplateCache> delegate;

    public HashMapTemplateCache(Optional<TemplateCache> delegate) {
        this.delegate = delegate;
    }

    @Override
    public synchronized Optional<FullPage> get(PageTitle title) throws CacheException {
        if (cache.containsKey(title)) {
            return Optional.ofNullable(cache.get(title));
        } else {
            return delegate.flatMap(c -> c.get(title)).map(page -> {
                cache.put(title, page);
                return page;
            });
        }
    }

    @Override
    public synchronized void store(FullPage page) throws CacheException {
        cache.put(page.getId().getTitle(), page);
        delegate.ifPresent(d -> d.store(page));
    }
}
