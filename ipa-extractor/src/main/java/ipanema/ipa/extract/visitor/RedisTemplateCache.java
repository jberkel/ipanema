package ipanema.ipa.extract.visitor;

import ipanema.ipa.extract.TemplateCache;
import org.sweble.wikitext.engine.FullPage;
import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.engine.PageTitle;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

import java.util.Optional;

public class RedisTemplateCache implements TemplateCache {
    private Jedis jedis;

    public RedisTemplateCache() {
        jedis = new Jedis("localhost");
    }

    @Override
    public Optional<FullPage> get(PageTitle title) throws TemplateCache.CacheException {
        String text;
        try {
            text = jedis.get(title.getDenormalizedFullTitle());
        } catch (JedisException e) {
            throw new CacheException(e);
        }

        if (text != null) {
            return Optional.of(new FullPage(new PageId(title, -1), text));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void store(FullPage page)throws CacheException {
        try {
            jedis.set(page.getId().getTitle().getDenormalizedFullTitle(), page.getText());
        } catch (JedisException e) {
            throw new CacheException(e);
        }
    }
}
