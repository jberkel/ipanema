package ipanema.ipa.extract.visitor;

import de.fau.cs.osr.ptk.common.AstVisitor;
import org.sweble.wikitext.engine.nodes.EngProcessedPage;
import org.sweble.wikitext.parser.nodes.WtNode;
import org.sweble.wikitext.parser.nodes.WtNodeList;
import org.sweble.wikitext.parser.nodes.WtSection;

abstract class PageVisitor extends AstVisitor<WtNode> {
    @SuppressWarnings("UnusedDeclaration")
    public void visit(WtNode n) {
    }

    @SuppressWarnings("UnusedDeclaration")
    public void visit(WtNodeList n) {
        iterate(n);
    }

    @SuppressWarnings("UnusedDeclaration")
    public void visit(WtSection n) {
        iterate(n);
    }

    @SuppressWarnings("UnusedDeclaration")
    public void visit(EngProcessedPage processedPage) {
        iterate(processedPage);
    }
}
