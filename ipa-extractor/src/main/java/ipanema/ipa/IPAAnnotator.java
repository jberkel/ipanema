package ipanema.ipa;

import ipanema.ipa.model.IPASound;
import ipanema.ipa.model.IPATree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IPAAnnotator {
    private IPATree ipaTree;

    public IPAAnnotator() {
        ipaTree = IPATree.fromJSON(getClass().getResourceAsStream("/ipa.json"));
    }

    public List<IPASound> annotate(String string) {
        if (string.isEmpty()) {
            return Collections.emptyList();
        }
        List<IPASound> sounds = new ArrayList<>();
        String current = string;
        while (current != null) {
            IPATree.Match match = ipaTree.find(current);
            match.sound.ifPresent(sounds::add);
            current = match.remaining;
        }
        return sounds;
    }
}
