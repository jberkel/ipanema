{{Infobox IPA
| above = Long
| ipa symbol = ◌ː
| ipa number =503
| decimal    =720
}}
{{Infobox IPA
| above = Half long
| ipa symbol = ◌ˑ
| ipa number =504
| decimal    =721
}}
{{Infobox IPA
| above = Extra long
| ipa symbol = ◌ːː
}}
In [[phonetics]], '''length''' or '''quantity''' is a [[distinctive feature|feature]] of sounds that have distinctively extended duration compared with other sounds. There are [[vowel length|long vowels]] as well as [[Consonant length|long consonants]] (the latter are often called ''geminates'').

Many languages do not have distinctive length. Among the languages that have distinctive length, there are only a few that have both distinctive vowel length and distinctive consonant length. It is more common that there is only one or that they depend on each other.

The languages that distinguish between different lengths have usually long and short sounds. According to some linguists, [[Estonian language|Estonian]] and some [[Sami languages]] have three phonemic (meaning-distinguishing) lengths for consonants and vowels. Some [[Low German]]/[[Low Saxon languages|Low Saxon]] varieties in the vicinity of [[Hamburg]]<ref>Stellmacher, 1973</ref> and some [[Moselle Franconian]]<ref>Page 116 in Elmar Ternes: ''{{lang|de|Einführung in die Phonologie.}}'' {{lang|de|Wissenschaftliche Buchgesellschaft}}, Darmstadt, 1987, ISBN 3-534-09576-6</ref> and [[Ripuiarian Franconian]] varieties do, too.

Strictly speaking, a pair of a long sound and a short sound should be identical except for their length. In certain languages, however, there are pairs of [[phoneme]]s that are traditionally considered to be long-short pairs even though they differ not only in length, but also in quality, for instance [[English language|English]] "long e" which is {{IPA|/iː/}} (as in ''f'''ee'''t'' {{IPA|/fiːt/}}) vs. "short i" which is {{IPA|/ɪ/}} (as in ''f'''i'''t'' {{IPA|/fɪt/}}) or [[German language|German]] "long e" which is {{IPA|/eː/}} (as in ''B'''ee'''t'' {{IPA|/beːt/}} 'garden bed') vs. "short e" which is {{IPA|/ɛ/}} (as in ''B'''e'''tt'' {{IPA|/bɛt/}} 'sleeping bed'). Also, tonal contour may reinforce the length, as in Estonian, where the over-long length is concomitant with a tonal variation resembling tonal stress marking.

In non-linear [[phonology]], the feature of length is often not a feature of a specific sound segment, but rather of the whole syllable.

==See also==
* [[Chroneme]]
* [[Extra-short]]

== References ==
{{cite book |first=Yallop Collin, Fletcher Janet|last=Clark John |year=2007 |title=Introduction to Phonetics and Phonology |chapter= |editor= |others= |pages=(pp)51–52, 26–27, 32–33 |location=Oxford |publisher=Blackwell |id= |url= |authorlink=}}
{{Reflist}}

{{Suprasegmentals}}

[[Category:Phonology]]
