{{infobox IPA
|ipa symbol=ʊ̈
|ipa symbol2=ʉ̞
|ipa number=321 415
|decimal1=650|decimal2=776
|x-sampa=U\ '''or''' }_o
|braille=of |braille2=4 |braille3=cc
}}
The '''near-close central rounded vowel''', or '''near-high central rounded vowel''', is a type of [[vowel]] sound, used in some spoken [[language]]s. The [[International Phonetic Alphabet]] can represent this sound in a number of ways (see the box on the right), but the most common symbols are {{angle bracket|{{IPA|ʊ̈}}}} ([[Centralization (phonetics)|centralized]] {{IPA|[ʊ]}}) and {{angle bracket|{{IPA|ʉ̞}}}} ([[lowered (phonetics)|lowered]] {{IPA|[ʉ]}}). The third edition of the [[OED]] adopted an unofficial extension of the IPA, {{angle bracket|{{IPA|ᵿ}}}}, that is a conflation of {{angle bracket|{{IPA|ʊ}}}} and {{angle bracket|{{IPA|ʉ}}}}, and represents either {{IPA|[ʊ̈]}} or free variation between {{IPA|[ʊ]}} and {{IPA|[ə]}}.

The IPA prefers terms "close" and "open" for vowels, and the name of the article follows this. However, a large number of linguists, perhaps a majority, prefer the terms "high" and "low".

==Features==
{{IPA chart vowels}}
{{near-close vowel}}
{{central vowel}}
{{rounded vowel}}
{{clear}}

==Occurrence==
{| class="wikitable"
! colspan="2" |Language !! Word !! [[International Phonetic Alphabet|IPA]]!! Meaning !! Notes
|-
| colspan="2" | [[Cornish language|Cornish]] || {{example needed}} || — || — ||
|-
| rowspan="2" | [[Dutch language|Dutch]] || Standard [[Flemish|Belgian]]<ref name="verhoeven">{{Harvcoltxt|Verhoeven|2005|p=245}}</ref> || {{lang|nl|[[Dutch orthography|''h'''u'''t'']]}} || {{Audio-IPA|Nl-hut.ogg|[ɦʊ̈t]}} || 'hut' || rowspan="2" | The Belgian vowel is somewhat lower, is typically transcribed as {{IPA|/ʏ/}} or {{IPA|/œ/}}, and it corresponds to {{IPAblink|ɵ}} in the Netherlands.<ref name="gussenhoven">{{Harvcoltxt|Gussenhoven|1992|p=47}}</ref> The Netherlandic vowel is typically transcribed {{IPA|/y/}}, and it corresponds to {{IPAblink|y}} in Belgium.<ref name="verhoeven"/> The latter has been also described as near-front {{IPAblink|ʏ}}.<ref>{{Harvcoltxt|Collins|Mees|2003|p=132}}</ref> See [[Dutch phonology]]
|-
| Netherlandic<ref name="gussenhoven"/> || {{lang|nl|[[Dutch orthography|''f'''uu'''t'']]}} || {{IPA|[fʊ̈t]}} || 'grebe'
|-
| rowspan="5" | [[English language|English]] || Some speakers || ''e'''u'''phoria'' || {{IPA|[jʊ̈ˈfɔə̯ɹiə]}} || 'euphoria' || [[Vowel reduction|Reduced]] form of the vowel {{IPA|/uː/}}, though may also be realized as {{IPA|[uː]}} or {{IPA|[ə]}}. See [[English phonology]].
|-
| [[Cockney]]<ref name="Mott">{{Harvcoltxt|Mott|2011|p=75}}</ref> || rowspan="4" | {{lang|en|[[English orthography|''g'''oo'''d'']]}} || rowspan="4" | {{IPA|[ɡʊ̈d]}} || rowspan="4" | 'good' || Only in some words, particularly ''good''.<ref name="Mott"/> Otherwise it's near-back {{IPAblink|ʊ}}.
|-
| Cultivated<br/>[[South African English|South African]]<ref>{{Harvcoltxt|Lass|2002|p=115-116}}</ref> || Younger, especially female speakers. Other speakers have a less front vowel {{IPAblink|ʊ}}
|-
| [[English in southern England|Southeastern English]]<ref>{{Harvcoltxt|Lodge|2009|p=174}}</ref> || May be unrounded {{IPAblink|ɪ̈}} instead; it corresponds to {{IPAblink|ʊ}} in other dialects. See [[English phonology]]
|-
| [[Hiberno-English|Ulster]]<ref>{{citeweb|title=Irish English and Ulster English|p=6|url=http://ifla.uni-stuttgart.de/institut/mitarbeiter/jilka/teaching/dialectology/d9_Ireland.pdf}}</ref> || Short allophone of {{IPA|/u/}}.
|-
| [[Irish language|Irish]] || [[Munster Irish|Munster]]<ref name="muir">{{Harvcoltxt|Ó Sé|2000}}</ref> || [[Irish orthography|''g'''io'''bal'']] || {{IPA|[ˈɟʊ̟bˠɰəɫ̪]}} || 'rag' || Slightly retracted;<ref name="muir"/> allophone of {{IPA|/ʊ/}} after a slender consonant.<ref name="muir"/> See [[Irish phonology]]
|-
| rowspan="2" | [[Norwegian language|Norwegian]] || Standard Eastern<ref>{{Harvcoltxt|Vanvik|1979|p=13}}</ref> || {{lang|no|''[[Norwegian alphabet|g'''u'''ll]]''}} || {{IPA|[ɡʊ̈l]}} || 'gold' || Somewhat fronted; can be transcribed {{IPA|/ʉ/}}. See [[Norwegian phonology]]
|-
| [[Norwegian dialects|Stavanger]]<ref>{{Harvcoltxt|Vanvik|1979|p=18}}</ref> || {{lang|no|''[[Norwegian alphabet|'''o'''nd]]''}} || {{IPA|[ʊ̈n]}} || 'bad' || Corresponds to {{IPAblink|ʊ}} in Standard Eastern Norwegian. See [[Norwegian phonology]]
|-
| colspan="2" | [[Russian language|Russian]]<ref>{{Harvcoltxt|Jones|Ward|1969|p=38}}</ref> || [[Russian orthography|'''ю'''титься]] || {{IPA|[jʊ̈ˈtʲit̪͡s̪ə]}} || 'to huddle' || Occurs only between [[Palatalization (phonetics)|palatalized]] consonants and in unstressed syllables. See [[Russian phonology]]
|}

==References==
{{reflist}}

==Bibliography==
{{refbegin}}
* {{citation
|last=Collins
|first=Beverley
|last2=Mees
|first2=Inger M.
|year= 2003
|title=The Phonetics of English and Dutch, Fifth Revised Edition
|isbn=9004103406
|url=http://npu.edu.ua/!e-book/book/djvu/A/iif_kgpm_Collins_Phonetics_of_English_and_Dutch_pdf.pdf
}}
* {{citation
|last=Gussenhoven
|first=Carlos
|year= 1992
|title=Dutch
|journal=Journal of the International Phonetic Association
|volume=22
|issue=2
|pages=45–47
|doi=10.1017/S002510030000459X
}}
*{{Citation
|last =Jones
|first= Daniel
|last2 =Ward
|first2= Dennis
|year= 1969
|title= The Phonetics of Russian
|publisher=Cambridge University Press
}}
* {{citation
|last=Lass
|first=Roger
|chapter=South African English
|editor-last=Mesthrie
|editor-first=Rajend
|year=2002
|title=Language in South Africa
|publisher=Cambridge University Press
|isbn=9780521791052
}}
*{{citation
|last=Lodge
|first=Ken
|year=2009
|title=A Critical Introduction to Phonetics
}}
*{{citation
|last=Mott
|first=Brian
|year=2011
|title=Traditional Cockney and Popular London Speech
|journal=Dialectologia
|volume=9
|pages=69-94
|issn=2013-2247
|url=http://diposit.ub.edu/dspace/bitstream/2445/37522/1/618486.pdf
}}
* {{citation
|surname=Ó Sé
|given=Diarmuid
|title=Gaeilge Chorca Dhuibhne
|place=Dublin
|publisher=Institiúid Teangeolaíochta Éireann
|year=2000
|isbn=0-946452-97-0
|language=ga
}}
*{{citation
|last=Vanvik
|first=Arne
|title=Norsk fonetik
|year=1979
|publisher=Universitetet i Oslo
|place=Oslo
|isbn=82-990584-0-6
}}
* {{citation
|last=Verhoeven
|first=Jo
|year= 2005
|title=Belgian Standard Dutch
|journal=Journal of the International Phonetic Association
|volume=35
|issue=2
|pages=245
|doi=10.1017/S0025100305002173
}}
{{refend}}

{{IPA navigation}}

[[Category:Vowels]]
