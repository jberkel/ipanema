{{Redirect-distinguish2|ⱱ|the Cyrillic letter [[Izhitsa]]}}
{{Infobox IPA
|ipa number=184
|decimal=11377
|imagefile=Labiodental flap (vector).svg
|imagesize=150px
|braille=235
|braille2=v
}}

The '''labiodental flap''' is a speech sound found primarily in languages of [[Central Africa]], such as [[Kera language|Kera]] and [[Mangbetu language|Mangbetu]]. It has also been reported in the [[Austronesian languages|Austronesian]] language [[Sika language|Sika]].<ref>{{Harvcoltxt|Olson|Hajek|2003|pp=162–164}}</ref> It is one of the few non-[[Rhotic consonant|rhotic]] flaps.

The sound begins with the lower lip placed behind the upper teeth. The lower lip is then flipped outward, striking the upper teeth in passing.<ref>{{Harvcoltxt|Olson|Hajek|1999|p=104}}</ref> The symbol in the [[International Phonetic Alphabet]] that represents this sound is {{angbr|{{IPA|ⱱ}}}}, which resembles Cyrillic [[izhitsa]], {{angbr|ѵ}}, but is composed of a vee and the hook of the flap {{angbr|{{IPA|ɾ}}}}.

When described in the literature, it is often transcribed by a ''v'' modified by the [[extra-short (phonetics)|extra-short]] diacritic, {{angbr|{{IPA|v̆}}}},<ref>{{Harvcoltxt|Olson|Hajek|2003|p=158}}</ref> following a recommendation of the International Phonetic Association.<ref>{{Harvcoltxt|International Phonetic Association|1989|p=70}}</ref> The ''[[:File:Labial flap.svg|v with a left loop]]'' symbol has been employed in articles from the [[School of Oriental and African Studies]] and by [[Joseph Greenberg]].<ref>{{Harvcoltxt|Olson|Hajek|1999|p=112}}</ref> In 2005 the [[International Phonetic Association]], responding to Dr. Kenneth S. Olson's request for its adoption, voted to include a symbol for this sound, and selected a ''v with a right hook''.<ref>{{Harvcoltxt|International Phonetic Association|2005|p=261}}</ref> This symbol is a combination of {{angbr|{{IPA|v}}}} + {{angbr|{{IPA|ɾ}}}} (the letters for the [[voiced labiodental fricative]] and the [[alveolar flap]]). As of version 5.1.0, the [[Unicode]] character set encodes this character at U+2C71 ({{Unicode|&#x2C71;}}).

==Occurrence==
The labiodental flap is found primarily in Africa, in perhaps a couple hundred languages in the [[Chadic languages|Chadic]] family (Margi, Tera), [[Ubangian languages|Ubangian]] (Ngbaka, Ma'bo, Sera), [[Central Sudanic]] (Mangbetu, Kresh), and [[Southern Bantoid languages|Bantoid]] (Ngwe, some Shona dialects). It is extremely rare outside Africa, though it has been reported from [[Sika language|Sika]] in [[Flores]].

{| class="wikitable"
!colspan=2| Language
! Word
! [[International Phonetic Alphabet|IPA]]
! Meaning
! Notes
|-
|colspan=2| [[Kera language|Kera]]
|colspan=2 align=center| {{IPA|[ⱱə̃ə̃ti]}}
| ‘push your head out of a hole or out of water’
| Since most of the lexical items are ideophones, we cannot say definitively that the labial flap has been fully incorporated into the phonological system of Kera.<ref>{{Harvcoltxt|Olson|Hajek|2003|p=27}}</ref>
|-
|colspan=2| [[Mangbetu language|Mangbetu]]
|colspan=2 align=center| {{IPA|[tɛⱱɛ]}}
| 'ten (10)'
|
|-
|colspan=2| [[Mono language (Congo)|Mono]]<ref>{{Harvcoltxt|Olson|2004|p=233}}</ref>
| [[Latin alphabet|'''''vw'''a'']]
|align=center| {{IPA|[ⱱa]}}
| 'send'
| Contrasts with {{IPA|/v/}} and {{IPA|/w/}}. In free variation with [[bilabial flap]]
|-
|colspan=2| [[Sika language|Sika]]
|colspan=2 align=center| {{IPA|[ⱱoːtɛr]}}
| 'I stand a pole in the ground'
| Contrasts with {{IPA|/v/}} and {{IPA|/β/}}. May also be realized as {{IPA|[b̪]}}
|}

The [[bilabial flap]] is a variant of the labiodental flap in several languages, including [[Mono language (Congo)|Mono]]. This sound involves striking the upper lip rather than the upper teeth. The two sounds are not known to contrast in any language; the term '''labial flap''' can be used as a broader description encompassing both sounds.<ref>{{Harvcoltxt|Olson|Hajek|1999|p=106}}</ref>

In [[Sika language|Sika]], the flap is heard in careful pronunciation, but it may also be realized as a [[voiced labiodental stop]], {{IPA|[b̪]}}, or an affricate. It contrasts with both a bilabial and a labiodental fricative:<ref>{{Harvcoltxt|Olson|Hajek|2003|p=181}}</ref>

{| class="wikitable"
| {{IPA|[ⱱoːtɛr]}}
| 'I stand a pole in the ground'
|-
| {{IPA|[βotɛːr]}}
| 'I buy'
|-
| {{IPA|[voːtɛr]}}
| 'We ([[Clusivity|inclusive]]) buy'
|}

==References==
{{reflist}}

==Bibliography==
*{{citation
|doi=10.1017/S0025100300003868
|author=International Phonetic Association
|year=1989
|title=Report on the 1989 Kiel Convention
|journal=Journal of the International Phonetic Association
|volume=19
|issue=2
|pages=67–80
}}
*{{citation
|doi=10.1017/S0025100305002227
|author=International Phonetic Association
|year=2005
|title=IPA news
|journal=Journal of the International Phonetic Association
|volume=35
|issue=2
|pages=261–262
}}
*{{citation
|doi=10.1017/S0025100300006484
|last=Olson
|first=Kenneth S
|last2=Hajek
|first2=John
|year=1999
|title=The phonetic status of the labial flap
|journal=Journal of the International Phonetic Association
|volume=29
|issue=2
|pages=101–114
}}
*{{citation
|doi=10.1515/lity.2003.014
|last=Olson
|first=Kenneth S
|last2=Hajek
|first2=John
|year=2003
|title=Crosslinguistic insights on the labial flap
|journal=Linguistic Typology
|volume=7
|issue=2
|pages=157–186
}}
*{{citation
|last=Olson
|first=Kenneth S
|last2=Hajek
|first2=John
|year=2004
|title=A crosslinguistic lexicon of the labial flap
|journal=Linguistic Discovery
|volume=2
|issue=2
|pages=21–57
|doi=10.1349/ps1.1537-0852.a.262
}}
*{{citation
|doi=10.1017/S0025100304001744
|last=Olson
|first=Kenneth
|year=2004
|title=Mono
|journal=Journal of the International Phonetic Association
|volume=34
|issue=2
|pages=233–238
}}

<!-- Please do not remove this unless the section is correctly configured with all the "missing" errors fixed
==Further reading==
{{citation
|last=Olson
|first=Kenneth
|last2=Schrag
|first2=Brian
|year=2000
|chapter=An overview of Mono phonology
|editor-last=Wolff
|editor-first=H.E.
|title=Proceedings from the 2nd World Congress of African Linguistics, Leipzig 1997
|place=Cologne
|publisher=Rüdiger Köppe
|pages=393–409
|editor3-last=Gensler
|editor3-first=O.
}} -->

==External links==
* Olson and Hajek, 2001. [http://www.sil.org/silewp/2001/002/silewp2001-002.pdf 'The Geographic and Genetic Distribution of the Labial Flap']
* [http://journals.dartmouth.edu/webobjbin/WebObjects/Journals.woa/1/xmlpage/1/article/262 A Crosslinguistic Lexicon of the Labial Flap] <small>(has video & sound files)</small>
* [http://www.sil.org/sil/news/2005/labiodental_flap.htm SIL Linguist Successfully Proposes New Phonetic Symbol]
* [http://www.sil.org/~olsonk/research.html Kenneth S. Olson's research website] <small>(has information on the labiodental flap)</small>

{{IPA navigation}}

[[Category:Labiodental consonants]]
[[Category:Flap consonants]]
[[Category:Phonetic transcription symbols]]
