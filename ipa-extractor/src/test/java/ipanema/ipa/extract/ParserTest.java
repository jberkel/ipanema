package ipanema.ipa.extract;


import org.apache.commons.io.IOUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.sweble.wikitext.engine.EngineException;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ParserTest {
    private Parser subject;

    @Before
    public void setUp() throws Exception {
        subject = new Parser();
    }

    @Ignore @Test(expected = EngineException.class)
    public void testAmbiguousPageThrowsException() throws Exception {
        parse("Ambiguous", "Ambiguous.txt");
    }

    @Test
    public void testParseOpenMid() throws Exception {
        IPAInfo info = parse("Open-mid back rounded vowel", "Open-mid_back_rounded_vowel.txt");
        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(1);
        IPAInfoBox box = parsed.get(0);
        assertThat(box.decimal[0]).isEqualTo(596);
        assertThat(box.number[0]).isEqualTo(306);
        assertThat(box.xsampa).isEqualTo("O");
        assertThat(box.kirshenbaum).isEqualTo("O");
        assertThat(box.imagefile).isEqualTo("Open-mid back rounded vowel (vector).svg");
        assertThat(box.braille).isEqualTo("gh");
        assertThat(box.braille2).isNull();
    }

    @Test
    public void testParseLabiodentalFlap() throws Exception {
        IPAInfo info = parse("Labiodental flap", "Labiodental_flap.txt");

        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(1);
        IPAInfoBox box = parsed.get(0);

        assertThat(box.decimal[0]).isEqualTo(11377);
        assertThat(box.number[0]).isEqualTo(184);
        assertThat(box.xsampa).isNull();
        assertThat(box.kirshenbaum).isNull();
        assertThat(box.imagefile).isEqualTo("Labiodental flap (vector).svg");
        assertThat(box.braille).isEqualTo("235");
        assertThat(box.braille2).isEqualTo("v");
    }

    @Test
    public void testDoubleIpaNumber() throws Exception {
        IPAInfo info = parse("Near-close central rounded vowel", "Near-close_central_rounded_vowel.txt");
        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(1);
        IPAInfoBox box = parsed.get(0);
        assertThat(box.number[0]).isEqualTo(321);
        assertThat(box.number[1]).isEqualTo(415);
    }

    @Test
    public void testPlusIpaNumber() throws Exception {
        IPAInfo info = parse("R-colored vowel", "/R-colored_vowel.txt");
        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(1);
        IPAInfoBox box = parsed.get(0);
        assertThat(box.number[0]).isEqualTo(322);
        assertThat(box.number[1]).isEqualTo(419);
    }

    @Test
    public void testComplexSymbol() throws Exception {
        IPAInfo info = parse("Voiceless palatal lateral fricative",  "Voiceless_palatal_lateral_fricative.txt");
        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(1);
        IPAInfoBox box = parsed.get(0);
        assertThat(box.decimal[0]).isEqualTo(654);
        assertThat(box.decimal[1]).isEqualTo(805);
        assertThat(box.decimal[2]).isEqualTo(724);
    }

    @Test
    public void testLength() throws Exception {
        IPAInfo info = parse("Length (Phonetics)", "Length_(phonetics).txt");
        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(3);
        assertThat(parsed.get(0).decimal[0]).isEqualTo(720);
        assertThat(parsed.get(1).decimal[0]).isEqualTo(721);
        assertThat(parsed.get(1).above).isEqualTo("Half long");
        assertThat(parsed.get(2).above).isEqualTo("Extra long");
    }

    @Test
    public void testVoicelessAlveolarFricative() throws Exception {
        IPAInfo info = parse("Voiceless alveolar fricative", "Voiceless_alveolar_fricative.txt");
        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(4);
        assertThat(parsed.get(0).above).isEqualTo("Voiceless alveolar sibilant");
        assertThat(parsed.get(0).decimal[0]).isEqualTo(115);
        assertThat(parsed.get(0).decimal[1]).isEqualTo(0);

        assertThat(parsed.get(1).above).isEqualTo("Voiceless laminal dentalized alveolar sibilant");
        assertThat(parsed.get(1).decimal[0]).isEqualTo(0);

        assertThat(parsed.get(2).above).isEqualTo("Voiceless alveolar retracted sibilant");
        assertThat(parsed.get(2).decimal[0]).isEqualTo(115);
        assertThat(parsed.get(2).decimal[1]).isEqualTo(826);

        assertThat(parsed.get(3).above).isNullOrEmpty();
        assertThat(parsed.get(3).decimal[0]).isEqualTo(952);
        assertThat(parsed.get(3).decimal[1]).isEqualTo(817);
        assertThat(parsed.get(3).number[0]).isEqualTo(130);
        assertThat(parsed.get(3).number[1]).isEqualTo(414);
    }

    @Test
    public void testUnrounded() throws Exception {
        IPAInfo info = parse("Near-close_near-front_unrounded_vowel.txt", "Near-close_near-front_unrounded_vowel.txt");
        final List<IPAInfoBox> parsed = info.getIPAInfo();
        Assertions.assertThat(parsed).hasSize(2);
        assertThat(parsed.get(0).decimal[0]).isEqualTo(618);
    }

    private String read(String file) throws IOException {
        String s = IOUtils.toString(getClass().getResourceAsStream(file));
        Assertions.assertThat(s).isNotEmpty();
        return s;
    }

    private IPAInfo parse(String name, String fixture) throws Exception {
        IPAInfo info = subject.parse(name, read(fixture.startsWith("/") ? fixture : "/"+fixture));
        assertThat(info.getArticleText()).isNotEmpty();
        assertThat(info.getArticleText()).doesNotContain("soft redirect");
        return info;
    }
}
