package ipanema.ipa;

import ipanema.ipa.model.IPASound;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class IPAAnnotatorTest {
    private IPAAnnotator subject;

    @Before
    public void setUp() throws Exception {
        subject = new IPAAnnotator();
    }

    @Test
    public void testAnnotateEmpty() throws Exception {
        List<IPASound> annotated = subject.annotate("");
        assertThat(annotated).isEmpty();
    }

    @Test
    public void testAnnotateWithStandardString() throws Exception {
        List<IPASound> annotated = subject.annotate("---!!!!-----KKKKKKQQQQ");
        assertThat(annotated).isEmpty();
    }

    @Test
    public void testAnnotateWithMultipleCodepointString() throws Exception {
        List<IPASound> annotated = subject.annotate("d͡ʒ");
        assertThat(annotated).hasSize(1);
        assertThat(annotated.get(0).name).isEqualTo("Voiced palato-alveolar affricate");
    }

    @Test
    public void testAnnotateWithS() throws Exception {
        List<IPASound> annotated = subject.annotate("s");
        assertThat(annotated).hasSize(1);
        assertThat(annotated.get(0).name).isEqualTo("Voiceless alveolar sibilant");
    }

    @Test
    public void testAnnotate() throws Exception {
        List<IPASound> annotated = subject.annotate("/ˈaɾ.vu.ɾɨ/");
        assertThat(annotated).isNotEmpty();
        assertThat(annotated.stream().map(s -> s.name).iterator()).contains(
                "Primary stress",
                "Open front unrounded vowel",
                "Alveolar tap",
                "Voiced labiodental fricative",
                "Close back rounded vowel",
                "Alveolar tap",
                "Close central unrounded vowel"
        );
    }

    @Test
    public void testAnnotateMissingFinalSlash() throws Exception {
        List<IPASound> annotated = subject.annotate("/ˈaɾ.vu.ɾɨ");
        assertThat(annotated).isNotEmpty();
        assertThat(annotated.stream().map(s -> s.name).iterator()).contains(
                "Primary stress",
                "Open front unrounded vowel",
                "Alveolar tap",
                "Voiced labiodental fricative",
                "Close back rounded vowel",
                "Alveolar tap",
                "Close central unrounded vowel"
        );
    }

}
