package ipanema.ipa.model;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

public class IPASoundTest {
    IPASound s1,s2, s3;

    @Before
    public void setUp() throws Exception {
        s1 = new IPASound("name1", "link1", new int[]{1,2}, new int[]{3,4});
        s2 = new IPASound("name2", "link2", new int[]{1,2}, new int[]{3,4});
        s3 = new IPASound("name3", "link3", new int[]{3,4}, new int[]{3,4});
    }

    @Test
    public void testEquals() throws Exception {
        assertThat(s1).isEqualTo(s2);
        assertThat(s1).isNotEqualTo(s3);
        assertThat(s2).isNotEqualTo(s3);
    }

    @Test
    public void testHashcode() throws Exception {
        assertThat(s1.hashCode()).isEqualTo(s2.hashCode());
        assertThat(s1.hashCode()).isNotEqualTo(s3.hashCode());
        assertThat(s2.hashCode()).isNotEqualTo(s3.hashCode());
    }
}
