local exports = {}

exports.getByCode = function (code)
    if code == "Hani" then
        return {
            getCharacters = function ()
                return (
                    "一-鿿"..
                    "㐀-䶿".. -- ExtA
                    "𠀀-𮯯".. -- SIP
                    "𰀀-𲎯".. -- ExtG-H
                    "﨎﨏﨑﨓﨔﨟﨡﨣﨤﨧﨨﨩"..
                    "⺀-⻿".. -- Radicals Supplement
                    "　-〿".. -- CJK Symbols and Punctuation
                    "𖿢𖿣𖿰𖿱".. -- Ideographic Symbols and Punctuation
                    "㇀-㇯".. -- Strokes
                    "㍻-㍿㋿" -- 組文字
                )
            end
        }
    end
end

return exports