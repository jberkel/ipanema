#!/usr/bin/env lua

require 'lua-nucleo'
local json = require 'dkjson'
local ipa = dofile(arg[1])

print(json.encode(ipa['symbols']))
