#!/usr/bin/env lua

require 'lua-nucleo'
dofile(arg[0]:match('.*/') .. 'mw.lua')
local json = require 'dkjson'
local lang_data = dofile(arg[1])
local fields = {
    'canonicalName',
    'wikidata_item',
    'family',            -- family for regular languages
    'scripts',           -- comma separated
    'parent'             -- only for etymology languages
}

local languages_data = require 'Module:languages/data'
local aliases = {}
for k, value in pairs(languages_data.aliases) do
    aliases[value] = k
end

for k, data in pairs(lang_data) do
    for i = 1, #fields do
        if data[i] then
            data[fields[i]] = data[i]
            data[i] = nil
        end
    end
    local wikidata_item = data['wikidata_item']
    if type(wikidata_item) == 'number' then
        data['wikidata_item'] = "Q" .. wikidata_item  -- normalize wikidata format
    end

    -- add (deprecated) global aliases
    if aliases[k] ~= nil then
        if type(data['aliases']) == 'table' then
            table.insert(data['aliases'], aliases[k])
        else
            data['aliases'] = { aliases[k] }
        end
    end

    -- remove empty values
    if next(data) == nil then
        lang_data[k] = nil
    end
end

print(json.encode(lang_data))
