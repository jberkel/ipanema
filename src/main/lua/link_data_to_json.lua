#!/usr/bin/env lua

require 'lua-nucleo'
local json = require 'dkjson'
dofile(arg[0]:match('.*/') .. 'mw.lua')

local link_data = dofile(arg[1])
print(json.encode(link_data))
