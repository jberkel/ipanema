#!/usr/bin/env lua

require 'lua-nucleo'
dofile(arg[0]:match('.*/') .. 'mw.lua')
local json = require 'dkjson'
local lang_data = dofile(arg[1])
local fields = {
    'canonicalName',
    'systems'
}

for k, data in pairs(lang_data) do
    for i = 1, #fields do
        if data[i] then
            data[fields[i]] = data[i]
            data[i] = nil
        end
    end

    -- remove empty values
    if next(data) == nil then
        lang_data[k] = nil
    end
end

print(json.encode(lang_data))
