rawset(_G, 'mw', {})

-- Convert codepoints to a string
--
-- @see string.char
-- @param ... int  List of codepoints
-- @return string
local function internalChar( t, s, e )
    local ret = {}
    for i = s, e do
        local v = t[i]
        if type( v ) ~= 'number' then
            checkType( 'char', i, v, 'number' )
        end
        v = math.floor( v )
        if v < 0 or v > 0x10ffff then
            error( S.format( "bad argument #%d to 'char' (value out of range)", i ), 2 )
        elseif v < 0x80 then
            ret[#ret + 1] = v
        elseif v < 0x800 then
            ret[#ret + 1] = 0xc0 + math.floor( v / 0x40 ) % 0x20
            ret[#ret + 1] = 0x80 + v % 0x40
        elseif v < 0x10000 then
            ret[#ret + 1] = 0xe0 + math.floor( v / 0x1000 ) % 0x10
            ret[#ret + 1] = 0x80 + math.floor( v / 0x40 ) % 0x40
            ret[#ret + 1] = 0x80 + v % 0x40
        else
            ret[#ret + 1] = 0xf0 + math.floor( v / 0x40000 ) % 0x08
            ret[#ret + 1] = 0x80 + math.floor( v / 0x1000 ) % 0x40
            ret[#ret + 1] = 0x80 + math.floor( v / 0x40 ) % 0x40
            ret[#ret + 1] = 0x80 + v % 0x40
        end
    end
    return string.char( unpack( ret ) )
end

mw['ustring'] = {}
mw['ustring']['char'] = function( ... )
    return internalChar( { ... }, 1, select( '#', ... ) )
end

mw['text'] = {}