#!/usr/bin/env python

import json
import os.path
import sys
from peewee import DoesNotExist

from ipanema.models import database, Language, LanguageAlias, LanguageFamily, \
    LanguageFamilyAlias, Script, StopWords


def create_schema():
    for model in [Language, LanguageAlias, LanguageFamily, LanguageFamilyAlias,
                  Script, StopWords]:
        model.drop_table(fail_silently=True)
        model.create_table()


def insert_scripts(scripts: dict[str, any]):
    for (code, data) in scripts.items():
        Script.create(code=code,
                      canonical_name=data['canonicalName'],
                      systems=data.get('systems', None),
                      direction=data.get('direction', None),
                      parent_script=data.get('parent', None))


def insert_families(families: dict[str, any]):
    for (code, data) in families.items():
        LanguageFamily.create(code=code,
                              canonical_name=data['canonicalName'],
                              parent_family=data.get('family', None),
                              proto_language_code=data.get('protoLanguage', None),
                              wikidata_item=data.get('wikidata_item', None))

        for alias in {name for name in data.get('aliases', []) + data.get('otherNames', [])}:
            LanguageFamilyAlias.create(code=code, name=alias)


# some families don't have an explicit proto language set although it exists
def resolve_implicit_proto_language():
    for family in LanguageFamily.select().where(LanguageFamily.proto_language_code.is_null()):
        try:
            proto_language = Language.get(Language.code == (family.code + '-pro'))
            family.proto_language_code = proto_language.code
            family.save()
        except DoesNotExist:
            pass


# pre-compute ancestor languages
def resolve_ancestor_language():
    def find_proto_language(language: Language, family: LanguageFamily):
        if family.proto_language_code and language.code != family.proto_language_code:
            return Language.query(family.proto_language_code)
        elif family.parent_family and family.parent_family.code != 'qfa-not':
            return find_proto_language(language, family.parent_family)
        else:
            return None

    for language in Language.select().where(Language.ancestor.is_null() &
                                            Language.family.is_null(False)):
        ancestor = find_proto_language(language, language.family)
        if ancestor:
            language.ancestor = ancestor
            language.save()


def insert_languages(data):
    def insert(code: str, data: dict[str, any]):
        ancestors = data.get('ancestors', [])
        match ancestors:
            # Due to the ongoing problems with Lua memory limits, this should be given as
            # a comma-separated list in a string (and not a table).
            case str(a_string):
                ancestors = [x.strip() for x in a_string.split(",")]
            case list():
                pass
            case _:
                raise Exception(f"Invalid ancestors: {ancestors}")

        # ignore multiple ancestors for now
        ancestor = ancestors[0] if len(ancestors) > 0 else None
        family = data.get('family', None)
        parent = data.get('parent', None)

        assert 'canonicalName' in data, f'missing canonical name in {code}'
        Language.create(code=code,
                        canonical_name=data['canonicalName'],
                        family=family,
                        type=data.get('type', None),
                        parent=parent,
                        ancestor=ancestor,
                        wikidata_item=data.get('wikidata_item', None))

    for (code, data) in data.items():
        insert(code, data)

        for alias in {name for name in data.get('aliases', []) + data.get('otherNames', [])}:
            LanguageAlias.create(code=code, name=alias)


def remove_language_duplicates():
    for language in Language.select(Language):
        duplicate = Language.select().where(
            (Language.code == language.canonical_name) &
            (Language.canonical_name != language.code)).first()

        if duplicate:
            for alias in LanguageAlias.select().where(LanguageAlias.code == duplicate.code):
                alias.delete_instance()
            duplicate.delete_instance()

    for alias in LanguageAlias.select().where(LanguageAlias.code == LanguageAlias.name):
        alias.delete_instance()


def insert_stopwords(stopwords):
    mappings = {
        'hr': 'sh',  # Croatian => Serbo-Croatian
        'ku': 'kmr'  # Kurdish => Northern Kurdish
    }
    for (code, words) in stopwords.items():
        StopWords.create(code=mappings.get(code, code),
                         source='iso',
                         word_list=json.dumps(words, ensure_ascii=False))


if __name__ == '__main__':
    if len(sys.argv) <= 4:
        print("%s lang_data.json lang_families.json "
              "stopwords.json output.sqlite" %
              os.path.basename(sys.argv[0]), file=sys.stderr)
        exit(1)

    lang_data = json.load(open(sys.argv[1], 'r'))
    lang_families = json.load(open(sys.argv[2], 'r'))
    scripts = json.load(open(sys.argv[3], 'r'))
    stopwords = json.load(open(sys.argv[4], 'r'))
    db_path = sys.argv[5]

    database.init(db_path)
    # database.pragma("foreign_keys", 0)
    with database.atomic():
        create_schema()
        insert_families(lang_families)
        insert_scripts(scripts)
        insert_languages(lang_data)
        remove_language_duplicates()
        resolve_implicit_proto_language()
        resolve_ancestor_language()
        insert_stopwords(stopwords)

    database.execute_sql('ANALYZE')
    database.execute_sql('VACUUM')
    database.close()
