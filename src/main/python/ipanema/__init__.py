from ipanema.models import database_connection, Language, LanguageFamily


def query_language(s):
    with database_connection():
        return Language.query(s)


def query_family(s):
    with database_connection():
        return LanguageFamily.query(s)
