from . import base
from peewee import TextField, ForeignKeyField, DoesNotExist, CompositeKey


class LanguageFamily(base.Model):
    code = TextField(primary_key=True, null=False)
    canonical_name = TextField(null=False)
    wikidata_item = TextField(index=True, null=True)
    proto_language_code = TextField(null=True)
    parent_family = ForeignKeyField('self', to_field='code', db_column='parent_family', null=True)

    class Meta:
        table_name = 'language_families'

    def __str__(self):
        return self.canonical_name

    def __repr__(self):
        return repr(self.object)

    @property
    def object(self):
        return {
            'code': self.code,
            'canonical_name': self.canonical_name,
            'wikidata_item': self.wikidata_item,
            'parent_family': str(self.parent_family),
            'proto_language_code': self.proto_language_code
        }

    @classmethod
    def query(cls, descriptor):
        """
        :param descriptor: a family name
                           a ISO 639-2 identifier or
                           a wikidata item id (Q...)
        :return: the family or None
        """
        try:
            return cls.get((cls.code == descriptor) |
                           (cls.canonical_name == descriptor) |
                           (cls.wikidata_item == descriptor))
        except DoesNotExist:
            try:
                return LanguageFamilyAlias.get(LanguageFamilyAlias.name == descriptor).family
            except DoesNotExist:
                return None


class LanguageFamilyAlias(base.Model):
    family = ForeignKeyField(LanguageFamily, to_field='code', db_column='code')
    name = TextField(null=False)

    class Meta:
        primary_key = CompositeKey('name', 'family')
        table_name = 'language_family_aliases'
