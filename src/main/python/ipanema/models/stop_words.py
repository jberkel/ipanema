from . import base, Language
from peewee import DoesNotExist, TextField, ForeignKeyField, CompositeKey
import json


class StopWords(base.Model):
    code = ForeignKeyField(Language, to_field='code', db_column='code')
    source = TextField(null=True)
    word_list = TextField(null=False)

    class Meta:
        table_name = 'stop_words'
        primary_key = CompositeKey('code', 'source')

    @classmethod
    def query(cls, descriptor):
        try:
            stop_words = cls.get(cls.code == descriptor)
            return json.loads(stop_words.word_list)
        except DoesNotExist:
            return []
