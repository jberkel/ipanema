from contextlib import contextmanager
from os import path, environ

import peewee
from pkg_resources import resource_filename
from playhouse.sqlite_ext import SqliteDatabase

if 'SHOW_SQL' in environ:
    import logging

    logger = logging.getLogger('peewee')
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(asctime)-15s  %(message)s'))
    logger.addHandler(handler)

database = SqliteDatabase(None, pragmas=[
    ('foreign_keys', '1'),
    ('defer_foreign_keys', '1')
])

DEFAULT_DATABASE = resource_filename(__name__, '../languages.sqlite')


@contextmanager
def database_connection(db_path=DEFAULT_DATABASE):
    if not path.exists(db_path):
        raise OSError("db not found: %s" % db_path)

    database.init(db_path)
    database.connect()
    yield database
    database.close()


class Model(peewee.Model):
    class Meta:
        database = database
        legacy_table_names = False

    @classmethod
    def sqlall(cls, safe=False):
        create_sql, _ = cls._schema._create_table(safe=safe).query()
        index_sql = [ctxt.query()[0] for ctxt in
                     cls._schema._create_indexes(safe=safe)]
        return [create_sql] + index_sql
