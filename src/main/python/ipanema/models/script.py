from . import base
from peewee import TextField, ForeignKeyField


class Script(base.Model):
    code = TextField(primary_key=True, null=False)
    canonical_name = TextField(null=False)
    systems = TextField(null=True)
    direction = TextField(null=True)
    parent_script = ForeignKeyField('self', to_field='code',
                                    db_column='parent_script', null=True)

    class Meta:
        table_name = 'scripts'

    def __str__(self):
        return self.canonical_name
