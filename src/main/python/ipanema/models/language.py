from . import base
from .language_family import LanguageFamily
from peewee import TextField, ForeignKeyField, DoesNotExist, CompositeKey


class Language(base.Model):
    # order of priority/availability: ISO 639-1, 639-3, 639-2
    # https://en.wiktionary.org/wiki/Wiktionary:Languages#Language_codes
    code = TextField(primary_key=True, null=False)
    canonical_name = TextField(null=False, index=True)
    parent = TextField(index=True, null=True)
    ancestor = ForeignKeyField('self', to_field='code',
                               db_column='ancestor', null=True,
                               deferrable='INITIALLY DEFERRED')
    family = ForeignKeyField(LanguageFamily, to_field='code',
                             db_column='family', null=True)
    type = TextField(index=True, null=True)
    wikidata_item = TextField(index=True, null=True)

    class Meta:
        table_name = 'languages'

    def __str__(self):
        return self.canonical_name

    def __repr__(self):
        return repr(self.object)

    @property
    def object(self):
        return {
            'code': self.code,
            'canonical_name': self.canonical_name,
            'family': self.family.object if self.family else None,
            'ancestor': str(self.ancestor),
            'parent': str(self.parent),
            'wikidata_item': self.wikidata_item
        }

    @classmethod
    def query(cls, descriptor):
        """
        :param descriptor: a language name (English),
                           a ISO 639-3 identifier or
                           a wikidata item id (Q...)
        :return: the language or None
        """
        try:
            return cls.get((cls.code == descriptor) |
                           (cls.canonical_name == descriptor) |
                           (cls.wikidata_item == descriptor))
        except DoesNotExist:
            try:
                return LanguageAlias.get(LanguageAlias.name == descriptor).language
            except DoesNotExist:
                return None

    @classmethod
    def select_with_ancestor(cls):
        Ancestor = Language.alias()
        return cls.select(Language, LanguageFamily, Ancestor) \
            .where(cls.ancestor.is_null(False)) \
            .join(Ancestor).switch(Language).join(LanguageFamily)


class LanguageAlias(base.Model):
    language = ForeignKeyField(Language, to_field='code', db_column='code')
    name = TextField(null=False)

    class Meta:
        primary_key = CompositeKey('language', 'name')
        table_name = 'language_aliases'
