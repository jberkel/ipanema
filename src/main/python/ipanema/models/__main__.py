from .language import Language, LanguageAlias
from .script import Script
from .language_family import LanguageFamily, LanguageFamilyAlias
from .stop_words import StopWords
import peewee

for model in peewee.sort_models([Language, LanguageFamily, LanguageAlias, LanguageFamilyAlias,
                                 Script, StopWords]):
    for sql in model.sqlall():
        print(sql + ';')
