from .models import database_connection, Language


def nodes_and_edges():
    edges = [(language, language.ancestor) for language in Language.select_with_ancestor()]
    nodes = {x for (n1, n2) in edges for x in (n1, n2)}
    return nodes, edges


# https://js.cytoscape.org/#notation/elements-json
def cytoscape(nodes, edges):
    families = {language.family for language in nodes if language.family is not None}
    data_families = [{
        'data': {
            'id': family.code,
            'label': family.canonical_name,
            **({'parent': family.parent_family.code} if family.parent_family else {})
        }
    } for family in families]
    data_nodes = [{
        'group': 'nodes',
        'data': {
            'id': language.code,
            'label': language.canonical_name,
            **({'parent': language.family.code} if language.family else {})
        }
    } for language in nodes]
    data_edges = [{
        'group': 'edges',
        'data': {
            'id': '%s:%s' % (source.code, target.code),
            'source': source.code,
            'target': target.code
        }
    } for (source, target) in edges]
    return data_nodes + data_edges + data_families


# https://gephi.org/gexf/format/
def gephi(nodes, edges):
    import xml.etree.ElementTree as ET

    gexf = ET.Element('gexf', xmlns='http://www.gexf.net/1.2draft', version='1.2')
    graph = ET.SubElement(gexf, 'graph', {'defaultEdgeType': 'directed', 'mode': 'static'})
    node_elements = ET.SubElement(graph, 'nodes')
    edge_elements = ET.SubElement(graph, 'edges')

    for node in nodes:
        ET.SubElement(node_elements, 'node', id=node.code, label=node.canonical_name)

    for (source, target) in edges:
        ET.SubElement(edge_elements, 'edge', id='%s:%s' % (source.code, target.code),
                      source=source.code,
                      target=target.code)

    return ET.tostring(gexf, encoding='unicode')


if __name__ == '__main__':
    import os
    import sys
    import json

    def help():
        print('%s [cytoscape|gephi]' % os.path.basename(sys.argv[0]))
        exit(1)

    if len(sys.argv) > 1:
        with database_connection():
            if sys.argv[1] == 'cytoscape':
                print(json.dumps(cytoscape(*nodes_and_edges()), indent=True))
            elif sys.argv[1] == 'gephi':
                print(gephi(*nodes_and_edges()))
            else:
                help()
    else:
        help()
