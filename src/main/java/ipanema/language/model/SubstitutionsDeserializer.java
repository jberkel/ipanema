package ipanema.language.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

class SubstitutionsDeserializer extends StdDeserializer<Substitutions> {
    protected SubstitutionsDeserializer() {
        super(Substitutions.class);
    }

    @Override
    public Substitutions deserialize(JsonParser parser,
                                    DeserializationContext ctxt) throws IOException {
        switch (parser.getCurrentToken()) {
            case START_OBJECT -> {
                final ObjectNode objectNode = parser.readValueAsTree();
                try {
                    return ctxt.readTreeAsValue(objectNode, Substitutions.class);
                } catch (IOException e) {
                    for (Iterator<Map.Entry<String, JsonNode>> it = objectNode.fields(); it.hasNext(); ) {
                        Map.Entry<String, JsonNode> entry = it.next();
                        if (entry.getValue() instanceof ObjectNode) {
                            return ctxt.readTreeAsValue(entry.getValue(), Substitutions.class);
                        }
                    }
                    return null;
                }
            }
            case VALUE_STRING -> {
                Substitutions entryName = new Substitutions();
                entryName.module = parser.getText();
                return entryName;
            }
            default -> {
                return null;
            }
        }
    }
}
