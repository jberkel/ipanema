package ipanema.language.model;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

class OverrideTransliterationDeserializer extends StdDeserializer<OverrideTransliteration> {
    protected OverrideTransliterationDeserializer() {
        super(OverrideTransliteration.class);
    }

    @Override
    public OverrideTransliteration deserialize(JsonParser parser, DeserializationContext deserializationContext) throws IOException {
        switch (parser.getCurrentToken()) {
            case VALUE_TRUE -> { return new OverrideTransliteration(true); }
            case VALUE_FALSE -> { return new OverrideTransliteration(false); }
            case VALUE_STRING -> { return new OverrideTransliteration(parser.getText()); }
            default -> {
                throw new JsonParseException(parser, "invalid data");
            }
        }
    }
}
