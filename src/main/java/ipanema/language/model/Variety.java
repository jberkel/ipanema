package ipanema.language.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class Variety implements Serializable {
    @Serial
    private static final long serialVersionUID = -7885868842330575815L;

    public Variety(String name) {
        this(name, new String[0]);
    }

    public Variety(String name, String[] aliases) {
        this.name = name;
        this.aliases = aliases;
    }

    String name;
    String[] aliases;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Variety variety = (Variety) o;
        return Objects.equals(name, variety.name)
            && Arrays.equals(aliases, variety.aliases);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(aliases);
        return result;
    }
}
