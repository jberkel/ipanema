package ipanema.language.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.fasterxml.jackson.databind.DeserializationFeature.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * A mapping of Wiktionary language data.
 *
 * @see <a href="https://en.wiktionary.org/wiki/Wiktionary:List_of_languages">Wiktionary:List_of_languages</a>
 * @see <a href="https://en.wiktionary.org/wiki/Category:Language_data_modules">Category:Language_data_modules</a>
 */
public class LanguageData {
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public static final Language ENGLISH = load().getLanguage("en").get();

    private static LanguageData data;

    private final Map<String, Language> byCode = new HashMap<>();
    private final Map<String, Language> byCanonicalName = new HashMap<>();
    private final Map<String, Family> familiesByCode = new HashMap<>();
    private final Map<String, Script> scriptsByCode = new HashMap<>();

    // "otherName" can map to more than one Language
    private final Map<String, List<Language>> otherNames = new HashMap<>();

    public static synchronized LanguageData load() {
        if (data == null) {
            try {
                data = fromJSON(
                    LanguageData.class.getResourceAsStream("/lang_data.json"),
                    LanguageData.class.getResourceAsStream("/lang_families.json"),
                    LanguageData.class.getResourceAsStream("/scripts_data.json")
                );
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return data;
    }

    /**
     * @return {@link Language} or {@link Family} corresponding to <code>spec</code>, or empty
     */
    public Optional<? extends Identifier> get(String spec) {
        final Optional<Language> language = getLanguage(spec);
        return language.isPresent() ? language : getFamily(spec);
    }

    /**
     * @param spec language code or name
     * @return {@link Language} matching <code>spec</code>, or empty
     */
    public Optional<Language> getLanguage(String spec) {
        if (byCode.containsKey(spec)) {
            return of(byCode.get(spec));
        } else if (byCanonicalName.containsKey(spec)) {
            return of(byCanonicalName.get(spec));
        } else if (otherNames.containsKey(spec)) {
            return of(otherNames.get(spec).get(0)); // always pick first one
        } else {
            return empty();
        }
    }

    /**
     * @param spec language code or name
     * @return {@link Family} matching <code>spec</code>, or empty
     */
    public Optional<Family> getFamily(String spec) {
        if (familiesByCode.containsKey(spec)) {
            return of(familiesByCode.get(spec));
        } else {
            return empty();
        }
    }

    /**
     * @param code language code or name
     * @return {@link Script} matching <code>spec</code>, or empty
     */
    public Optional<Script> getScript(String code) {
        if (scriptsByCode.containsKey(code)) {
            return of(scriptsByCode.get(code));
        } else {
            return empty();
        }
    }

    /**
     * Convenience method, delegates to {@link Language#makeEntryName}.
     */
    public String makeEntryName(String languageCode, String term) {
        return getLanguage(languageCode).map(l -> l.makeEntryName(term)).orElse(term);
    }

    public int size() {
        return byCode.size() + familiesByCode.size();
    }

    /* package */ static LanguageData fromJSON(
            InputStream languagesJSON,
            InputStream familiesJSON,
            InputStream scriptsJSON
        ) throws IOException {
        final ObjectMapper mapper = new ObjectMapper()
                .enable(FAIL_ON_UNKNOWN_PROPERTIES);
        LanguageData data = new LanguageData();

        // languages
        Map<String, Language> languageMap =  mapper.reader().readValue(
            mapper.getFactory().createParser(languagesJSON),
            new TypeReference<>() {});

        // scripts
        Map<String, Script> scriptMap = mapper.reader().readValue(
            mapper.getFactory().createParser(scriptsJSON),
            new TypeReference<>() {});
        data.scriptsByCode.putAll(scriptMap);

        // families
        Map<String, Family> familyMap = mapper.reader().readValue(
            mapper.getFactory().createParser(familiesJSON),
            new TypeReference<>() {});

        for (Map.Entry<String, Family> e : familyMap.entrySet()) {
            final Family family = e.getValue();
            final String code = e.getKey();

            family.code = code;
            family.family = familyMap.get(family.parent);

            if (family.protoLanguageCode != null) {
                family.protoLanguage = languageMap.get(family.protoLanguageCode);
            }

            data.familiesByCode.put(code, family);
        }

        // languages
        for (Map.Entry<String, Language> entry : languageMap.entrySet()) {
            final Language language = entry.getValue();
            final String code = entry.getKey();

            language.code = code;
            language.family = familyMap.get(language.familyCode);


            if (language.parent != null) {
                language.parentIdentifier = languageMap.get(language.parent);
                if (language.parentIdentifier == null) {
                    language.parentIdentifier = familyMap.get(language.parent);
                }
            }

            data.byCode.put(code, language);
            data.byCanonicalName.put(language.canonicalName, language);

            if (language.otherNames != null) {
                for (String otherName : language.otherNames) {
                    final List<Language> languages = data.otherNames.computeIfAbsent(
                        otherName,
                        ignored -> new ArrayList<>());

                    if (!languages.contains(language)) {
                        languages.add(language);
                    }
                }
            }
            if (language.aliases != null) {
                for (String otherName : language.aliases) {
                    final List<Language> languages = data.otherNames.computeIfAbsent(
                            otherName,
                            ignored -> new ArrayList<>());

                    if (!languages.contains(language)) {
                        languages.add(language);
                    }
                }
            }
            if (language.aliasesCodes != null) {
                for (String aliasCode : language.aliasesCodes) {
                    data.byCode.put(aliasCode, language);
                }
            }
        }
        return data;
    }
}
