package ipanema.language.model;

public class OverrideTransliteration {
    public final Boolean override;
    public final String scriptCodes;

    public OverrideTransliteration(boolean override) {
        this.override = override;
        this.scriptCodes = null;
    }

    public OverrideTransliteration(String scriptCodes) {
        this.override = null;
        this.scriptCodes = scriptCodes;
    }
}
