package ipanema.language.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

class SortKeyDeserializer extends StdDeserializer<Language.SortKey> {
    public SortKeyDeserializer() {
        super(Language.SortKey.class);
    }

    @Override
    public Language.SortKey deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        parser.skipChildren();
        return null;
    }
}
