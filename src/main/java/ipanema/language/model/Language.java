package ipanema.language.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serial;
import java.io.Serializable;
import java.text.Normalizer;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NON_PRIVATE;
import static ipanema.language.model.Identifier.Type.ETYMOLOGY_LANGUAGE;
import static ipanema.language.model.Identifier.Type.REGULAR;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * @see <a href="https://en.wiktionary.org/wiki/Module:languages/data2">Module:languages/data2</a>
 * @see <a href="https://en.wiktionary.org/wiki/Template:language_extradata_documentation">Template:language_extradata_documentation</a>
 */
@JsonAutoDetect(fieldVisibility = NON_PRIVATE)
public class Language implements Identifier {
    @Serial
    private static final long serialVersionUID = 1570422711347253547L;
    @JsonProperty("code")
    String code;
    @JsonProperty("canonicalName")
    String canonicalName;
    @JsonIgnore
    Family family;
    @JsonProperty("family")
    String familyCode;
    @JsonProperty("wikidata_item")
    String wikidataItem;

    @JsonProperty("wikipedia_article")
    String wikipediaArticle;

    @JsonProperty("display_text")
    @JsonDeserialize(using = DisplayTextDeserializer.class)
    DisplayText displayText;

    @JsonProperty("sort_key")
    @JsonDeserialize(using = SortKeyDeserializer.class)
    SortKey sortKey;

    Identifier parentIdentifier;

    @JsonProperty("parent")
    String parent;

    Type type;

    List<String> otherNames;
    List<String> aliases;
    @JsonProperty("alias_codes")
    List<String> aliasesCodes;

    @JsonDeserialize(using = CommaSeparatedDeserializer.class)
    List<String> ancestors;

    @JsonDeserialize(using = CommaSeparatedDeserializer.class)
    List<String> scripts;

    @JsonDeserialize(using = SubstitutionsDeserializer.class)
    @JsonProperty("entry_name")
    Substitutions entryName;

    // Set this to true to make the automatic transliteration override an any given
    // manual transliteration. Otherwise, this can be a comma-separated list of
    // script codes, which means that the override is only applied to terms using those
    // scripts.
    @JsonProperty("override_translit")
    @JsonDeserialize(using = OverrideTransliterationDeserializer.class)
    OverrideTransliteration overrideTransliteration;

    @JsonDeserialize(using = SubstitutionsDeserializer.class)
    @JsonProperty("translit")
    Substitutions transliteration;

    @JsonDeserialize(using = StandardCharsDeserializer.class)
    StandardChars standardChars;

    @JsonDeserialize(contentUsing = VarietyDeserializer.class)
    List<Variety> varieties;

    @JsonProperty("ietf_subtag")
    String ietfSubtag;

    /**
     * Set this to true for languages that distinguish between the dotted and dotless I
     * (such as some Turkic languages).
     */
    @JsonProperty("dotted_dotless_i")
    boolean dottedAndDotlessI;

    /**
     * The <a href="https://en.wiktionary.org/wiki/Wiktionary:Wikimedia_language_codes">
     * Wikimedia language codes</a> that this language maps to.
     */
    @JsonProperty("wikimedia_codes")
    @JsonDeserialize(using = CommaSeparatedDeserializer.class)
    List<String> wikimediaCodes;

    /**
     * Set this to true to link the language's transliteration.
     */
    @JsonProperty("link_tr")
    boolean linkTr;

    @JsonProperty("ancestral_to_parent")
    boolean ancestralToParent;

    @JsonProperty("generate_forms")
    String generateForms;

    @JsonProperty("preprocess_links")
    String preprocessLinks;

    @JsonProperty("translit_module")
    String translitModule;

    @Override
    public String toString() {
        return "Language{" +
                "code='" + code + '\'' +
                ", canonicalName='" + canonicalName + '\'' +
                ", family='" + family + '\'' +
                ", otherNames=" + otherNames +
                ", aliases=" + aliases +
                ", ancestors=" + ancestors +
                ", scripts=" + scripts +
                '}';
    }

    public static class DisplayText implements Serializable {
    }

    public static class SortKey implements Serializable {
    }

    /**
     * @return the language code of the language. Example: "fr" for French.
     */
    public String getCode() {
        return code;
    }

    /**
     * @return Returns the canonical name of the language. This is the name used to
     * represent that language on Wiktionary, and is guaranteed to be unique to that
     * language alone. Example: "French" for French.
     */
    public String getCanonicalName() {
        return canonicalName;
    }

    /**
     * @return returns a Family object for the language family that the language belongs to.
     */
    public Optional<Family> getFamily() {
        return family == null ? empty() : of(family);
    }

    /**
     * @return Returns the Wikidata item id for the language or null
     */
    public String getWikidataItem() {
        return wikidataItem;
    }

    public Optional<? extends Identifier> getParent() {
        return parentIdentifier == null ?
            (getType() == ETYMOLOGY_LANGUAGE ? getFamily() : empty()) : of(parentIdentifier);
    }

    public Type getType() {
        return type == null ? REGULAR : type;
    }

    public List<String> getOtherNames() {
        return otherNames == null ? emptyList() : otherNames;
    }

    public List<String> getAliases() {
        return aliases == null ? emptyList() : aliases;
    }

    public List<String> getAncestors() {
        return ancestors == null ? emptyList() : ancestors;
    }

    public List<String> getScripts() {
        return scripts == null ? emptyList() : scripts;
    }

    public Optional<Substitutions> getEntryName() {
        return entryName == null ? empty() : of(entryName);
    }

    /**
     * @param term to convert
     * @return Converts the given term into the form used in the names of entries.
     *  This removes diacritical marks from the term if they are not considered part of the normal written
     *  form of the language, and which therefore are not permitted in page names.
     * @see <a href="https://en.wiktionary.org/wiki/Module:languages#Language:makeEntryName">Module:languages#Language:makeEntryName</a>
     */
    public String makeEntryName(String term) {
        final String cleaned = term
                .replaceAll("^[¿¡]", "")
                .replaceAll("(.)[؟?!;՛՜ ՞ ՟？！︖︕।॥။၊་།]$", "$1");

        return getEntryName().flatMap(entryName -> {
            String normalized = cleaned;

            if (!entryName.getFrom().isEmpty()) {
                for (int i = 0; i < entryName.getFrom().size(); i++) {
                    final String from = entryName.getFrom().get(i);
                    final String to = i < entryName.getTo().size() ? entryName.getTo().get(i) : "";

                    normalized = Pattern.compile(from)
                            .matcher(normalized)
                            .replaceAll(to.replace("%", "$"));
                }
            }
            if (entryName.getRemoveDiacritics() != null) {
                normalized = Normalizer.normalize(normalized, Normalizer.Form.NFD);
                normalized = normalized.replaceAll("[" + entryName.getRemoveDiacritics() + "]", "");
                normalized = Normalizer.normalize(normalized, Normalizer.Form.NFC);
            }
            return of(normalized);
        }).orElse(cleaned);
    }

    // Right now, equality is only based on "canonicalName", since it is
    // the only field guaranteed to be unique.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Language language = (Language) o;
        return Objects.equals(canonicalName, language.canonicalName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(canonicalName);
    }
}
