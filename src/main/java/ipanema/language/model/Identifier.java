package ipanema.language.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;
import java.util.Optional;

import static java.util.Optional.of;

public interface Identifier extends Serializable {
    long serialVersionUID = 7177572193103397573L;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    enum Type {
        REGULAR("regular"),
        RECONSTRUCTED("reconstructed"),
        APPENDIX_CONSTRUCTED("appendix-constructed"),
        ETYMOLOGY_LANGUAGE("etymology language"),
        FAMILY("family");

        public final String name;

        Type(String name) {
            this.name = name;
        }

        @JsonCreator
        static Type fromJsonString(JsonNode s) {
            for (Type t : values()) {
                if (s.asText().equalsIgnoreCase(t.name)) {
                    return t;
                }
            }
            return REGULAR;
        }
    }

    String getCode();
    String getCanonicalName();

    Type getType();

    /**
     * @return the parent can be a language or family
     */
    Optional<? extends Identifier> getParent();

    String getWikidataItem();

    /**
     * If language is an etymology language, iterates through parents
     * until it finds a non-etymology language (or family in some cases).
     *
     * @see <a href="https://en.wiktionary.org/wiki/Module:etymology">Module:etymology</a>
     */
    default Optional<? extends Identifier> getNonEtymological() {
        if (getType() == Type.ETYMOLOGY_LANGUAGE) {
            return getParent().flatMap(Identifier::getNonEtymological);
        } else {
            return of(this);
        }
    }
}
