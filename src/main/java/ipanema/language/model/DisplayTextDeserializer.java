package ipanema.language.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

class DisplayTextDeserializer extends StdDeserializer<Language.DisplayText> {
    public DisplayTextDeserializer() {
        super(Language.DisplayText.class);
    }

    @Override
    public Language.DisplayText deserialize(JsonParser parser,
                                            DeserializationContext ctxt) throws IOException {
        parser.skipChildren();
        return null;
    }
}
