package ipanema.language.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;

class VarietyDeserializer extends StdDeserializer<Variety> {
    public VarietyDeserializer() {
        super(Variety.class);
    }

    @Override
    public Variety deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        switch (parser.currentToken()) {
            case VALUE_STRING -> {
                return new Variety(parser.getText(), new String[0]); }
            case START_ARRAY -> {
                ArrayNode array = parser.readValueAsTree();
                if (array.isEmpty()) {
                    return null;
                } else {
                    String main = array.get(0).asText();
                    String[] alternatives = new String[array.size()-1];
                    for (int i=0; i<array.size()-1; i++) {
                        alternatives[i] = array.get(i+1).asText();
                    }
                    return new Variety(main, alternatives);
                }
            }
            default -> {
                return null;
            }
        }
    }
}
