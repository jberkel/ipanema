package ipanema.language.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serial;
import java.util.List;
import java.util.Optional;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NON_PRIVATE;
import static ipanema.language.model.Identifier.Type.FAMILY;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * @see <a href="https://en.wiktionary.org/wiki/Module:families">Module:families</a>
 */
@JsonAutoDetect(fieldVisibility = NON_PRIVATE)
public class Family implements Identifier {
    @Serial
    private static final long serialVersionUID = 6811320826950056268L;
    String code;
    String canonicalName;

    @JsonProperty("family") String parent;

    @JsonProperty("wikidata_item")
    String wikidataItem;

    @JsonIgnore Family family;

    Language protoLanguage;
    @JsonProperty("protoLanguage") String protoLanguageCode;

    List<String> otherNames;
    List<String> aliases;

    @JsonProperty("type")
    String declaredType;

    /**
     * A table of family varieties that are subsumed under the family. This should not include those varieties
     * for which separate family codes have been assigned. If a given variety has several names, they can all be
     * listed by including a sublist in the overall list, where the first element is the canonical name that you
     * want the variety to be known by, and the remainder are aliases
     */
    List<String> varieties;

    /**
     * @return the family code of the family. Example: "ine" for the Indo-European languages.
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the canonical name of the family. This is the name used to represent
     * that language family on Wiktionary, and is guaranteed to be unique to that family alone.
     * Example: "Indo-European" for the Indo-European languages.
     */
    public String getCanonicalName() {
        return canonicalName;
    }

    @Override
    public Type getType() {
        return FAMILY;
    }

    /**
     * @return a Family object for the parent family that the family is a part of.
     */
    public Optional<Family> getFamily() {
        return family == null ? empty() : of(family);
    }

    /**
     * @return the Wikidata item of that family.
     */
    public String getWikidataItem() {
        return wikidataItem;
    }

    public Optional<? extends Identifier> getParent() {
        return getFamily();
    }

    /**
     * @return a Language object for the proto-language of this family,
     * if one exists.
     */
    public Optional<Language> getProtoLanguage() {
        return protoLanguage == null ? empty() : of(protoLanguage);
    }

    public List<String> getOtherNames() {
        return otherNames == null ? emptyList() : otherNames;
    }

    public List<String> getAliases() {
        return aliases == null ? emptyList() : aliases;
    }
}
