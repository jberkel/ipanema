package ipanema.language.model;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class CommaSeparatedDeserializer extends StdDeserializer<List<String>> {
    public CommaSeparatedDeserializer() {
        super(List.class);
    }

    @Override
    public List<String> deserialize(JsonParser parser,
                                    DeserializationContext ctxt) throws IOException, JacksonException {
        if (parser.hasToken(JsonToken.VALUE_STRING)) {
            String text = parser.getText();
            if (text.isBlank()) {
                return Collections.emptyList();
            }
            List<String> list = new ArrayList<>();
            for (String s : text.split(",")) {
                list.add(s.trim());
            }
            return list;
        } else if (parser.hasToken(JsonToken.START_ARRAY)) {
            /*
            ArrayNode array = parser.readValueAsTree();
            List<String> list = new ArrayList<>();
            for (JsonNode node : array) {
                list.add(node.asText());
            }
            return list;
             */
            throw new JsonParseException(parser, "Invalid parser state");
        } else {
            throw new JsonParseException(parser, "Invalid parser state");
        }
    }
}
