package ipanema.language.model;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

class StandardCharsDeserializer extends JsonDeserializer<StandardChars> {
    @Override
    public StandardChars deserialize(JsonParser parser,
                                     DeserializationContext ctxt) throws IOException {
        switch (parser.getCurrentToken()) {
            case START_OBJECT -> {
                StandardChars standardChars = new StandardChars();
                final ObjectNode objectNode = parser.readValueAsTree();
                for (Iterator<Map.Entry<String, JsonNode>> it = objectNode.fields(); it.hasNext(); ) {
                    Map.Entry<String, JsonNode> entry = it.next();
                    if (entry.getValue().isTextual()) {
                        standardChars.entries.put(entry.getKey(), entry.getValue().textValue());
                    }
                } return standardChars;
            } case VALUE_STRING -> {
                StandardChars standardChars = new StandardChars();
                standardChars.defaultValue = parser.getText();
                return standardChars;
            }
            default -> {
                return null;
            }
        }
    }
}
