package ipanema.language.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NON_PRIVATE;
import static java.util.Collections.emptyList;

@JsonAutoDetect(fieldVisibility = NON_PRIVATE)
public class Substitutions implements Serializable {
    @Serial
    private static final long serialVersionUID = 1909721444316776056L;

    List<String> from;
    List<String> to;

    // The name of a module (for instance, "ar-entryname" referring to `Module:ar-entryname`),
    // the module must contain a entry name generating function that is named `makeEntryName`
    String module;

    @JsonProperty("remove_diacritics")
    String removeDiacritics;

    // Prevents specific characters from having their diacritics stripped.
    @JsonProperty("remove_exceptions")
    List<String> removeExceptions;

    public List<String> getFrom() {
        return from == null ? emptyList() : from;
    }

    public List<String> getTo() {
        return to == null ? emptyList() : to;
    }

    public String getRemoveDiacritics() {
        return removeDiacritics;
    }
}