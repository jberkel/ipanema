package ipanema.language.model;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NON_PRIVATE;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @see <a href="https://en.wiktionary.org/wiki/Module:scripts/data">Module:scripts/data</a>
 */
@JsonAutoDetect(fieldVisibility = NON_PRIVATE)
public class Script implements Serializable {
  @Serial
  private static final long serialVersionUID = -5284001708914301385L;

  //  The "canonical" name of the script. This is the name that is used in Wiktionary entries
  // and category names.
  String canonicalName;

  // A list of aliases/synonyms for the script, other than the canonical name.
  List<String> aliases;

  // The categories of writing system to which the script belongs.
  @JsonDeserialize(using = CommaSeparatedDeserializer.class)
  List<String> systems;

  // Describes the direction of writing. Most scripts are "ltr" (left-to-right) so they can be omitted.
  String direction;

  String parent;

  boolean capitalized;
  boolean spaces;
  @JsonProperty("sort_by_scraping")
  boolean sortByScraping;
  boolean translit;

  // Used by Module:character info. If false, prevents {{character info}} from adding a characters
  // category, such as Category:Latin script characters.
  @JsonProperty("character_category")
  String characterCategory;

  // A Lua character class that matches on any character that belongs to this script.
  String characters;

  @JsonProperty("wikipedia_article")
  String wikipediaArticle;

  @Deprecated
  List<String> otherNames;

  // A table of script varieties that are subsumed under the script.
  @JsonDeserialize(contentUsing = VarietyDeserializer.class)
  List<Variety> varieties;

  NormalizationFixes normalizationFixes;

  List<Integer> ranges;

  @JsonAutoDetect(fieldVisibility = NON_PRIVATE)
  public static class NormalizationFixes {
    List<String> from;
    List<String> to;
    Map<String, Integer> combiningClasses;
  }
}
