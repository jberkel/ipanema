package ipanema.links;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @see <a href="https://www.mediawiki.org/wiki/Manual:Interwiki">Manual:Interwiki</a>
 */
public class Interwiki implements Serializable {
    private static final long serialVersionUID = 6811320826950056268L;
    /**
     * Is the prefix of the interwiki link; this is used the same way as a namespace is used when editing.
     * @since 1.11
     */
    String prefix;

    /**
     * The URL of the wiki, with "$1" as a placeholder for an article name.
     * @since 1.11
     */
    String url;

    /**
     * If the interwiki prefix is a language code defined in <code>Language::fetchLanguageNames()</code>
     * from <code>$wgExtraLanguageNames</code>, this will be the name of that language.
     */
    String language;

    /**
     * Whether the interwiki prefix points to a site belonging to the current wiki farm. The value is
     * read straight out of the iw_local column of the interwiki table.
     * @since 1.11
     */
    boolean local;

    /**
     * Whether the interwiki link points to the current wiki, based on
     * <code>Manual:$wgLocalInterwikis</code>.
     * @since 1.24
     */
    @JsonProperty
    boolean localinterwiki;

    /**
     * Whether the value of url can be treated as protocol-relative. Note, however, that the URL actually
     * returned will always include the current protocol.
     * @since 1.24
     */
    @JsonProperty
    boolean protorel;


    public String getPrefix() {
        return prefix;
    }

    public String getUrl() {
        return url;
    }

    public String getLanguage() {
        return language;
    }

    public boolean isLocal() {
        return local;
    }

    public boolean isLocalInterwiki() {
        return localinterwiki;
    }

    public boolean isProtocolRelative() {
        return protorel;
    }

    @Override
    public String toString() {
        return "Interwiki{" +
            "prefix='" + prefix + '\'' +
            ", url='" + url + '\'' +
            ", language='" + language + '\'' +
            ", local=" + local +
            ", localinterwiki=" + localinterwiki +
            ", protorel=" + protorel +
        '}';
    }
}
