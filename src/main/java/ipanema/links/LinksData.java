package ipanema.links;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static java.util.Objects.requireNonNull;

public class LinksData {
    private static LinksData data;

    @JsonProperty("unsupported_titles")
    private Map<String, String> unsupportedTitles;

    public @Nullable String geReplacementTitle(String title) {
        return getUnsupportedTitles().contains(title) ? "Unsupported titles/" + unsupportedTitles.get(title) : null;
    }

    public Set<String> getUnsupportedTitles() {
        return unsupportedTitles.keySet();
    }

    public static synchronized LinksData load() {
        if (data == null) {
            try {
                data = fromJSON(LinksData.class.getResourceAsStream("/links_data.json"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return data;
    }

    private static LinksData fromJSON(InputStream linksJSON) throws IOException {
        requireNonNull(linksJSON);

        final ObjectMapper mapper = new ObjectMapper()
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false);

        return mapper.reader().readValue(mapper.getFactory().createParser(linksJSON), LinksData.class);
    }
}
