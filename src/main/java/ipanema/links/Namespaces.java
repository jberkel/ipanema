package ipanema.links;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static ipanema.links.Link.unescapeSpace;
import static java.util.Objects.requireNonNull;

public class Namespaces {
    private static Namespaces namespaces;
    private Map<Integer, Namespace> byId = new HashMap<>();
    private Map<String, Namespace> byName = new HashMap<>();

    /**
     * @return All known namespace names, including aliases.
     */
    public Set<String> getNames() {
        return byName.values().stream().flatMap(namespace ->
            Stream.of(namespace.getName(), namespace.getCanonical())
        ).collect(Collectors.toSet());
    }

    public Namespace get(String name) {
        return name == null ? null : byName.get(unescapeSpace(name).toLowerCase());
    }

    public int size() {
        return byId.size();
    }

    public static synchronized Namespaces load() {
        if (namespaces == null) {
            try {
                namespaces = fromJSON(Namespaces.class.getResourceAsStream("/namespaces.json"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return namespaces;
    }

    private static Namespaces fromJSON(InputStream namespacesJSON) throws IOException {
        requireNonNull(namespacesJSON);

        final ObjectMapper mapper = new ObjectMapper()
                .configure(FAIL_ON_UNKNOWN_PROPERTIES, false);

        final TypeReference<List<Namespace>> ref = new TypeReference<List<Namespace>>() {};
        final Namespaces namespaces = new Namespaces();
        final List<Namespace> namespaceList = mapper.reader().readValue(mapper.getFactory().createParser(namespacesJSON), ref);
        for (Namespace entry : namespaceList) {
            namespaces.byId.put(entry.id, entry);
            namespaces.byName.put(entry.getName().toLowerCase(), entry);
            if (entry.getCanonical() != null) {
                namespaces.byName.put(entry.getCanonical().toLowerCase(), entry);
            }
        }
        return namespaces;
    }
}
