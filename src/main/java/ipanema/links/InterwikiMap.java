package ipanema.links;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static java.util.Objects.requireNonNull;

public class InterwikiMap {
    private Map<String, Interwiki> byPrefix = new HashMap<>();
    private static InterwikiMap map;

    public Interwiki get(String prefix) {
        return prefix == null ? null : byPrefix.get(prefix.toLowerCase().trim());
    }

    public Set<String> getPrefixes() {
        return byPrefix.keySet();
    }

    public int size() {
        return byPrefix.size();
    }

    public static synchronized InterwikiMap load() {
        if (map == null) {
            try {
                map = fromJSON(InterwikiMap.class.getResourceAsStream("/interwikimap.json"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return map;
    }

    private static InterwikiMap fromJSON(InputStream interwikimapJSON) throws IOException {
        requireNonNull(interwikimapJSON);

        final ObjectMapper mapper = new ObjectMapper()
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false);

        final TypeReference<List<Interwiki>> ref = new TypeReference<List<Interwiki>>() {};
        final InterwikiMap map = new InterwikiMap();
        final List<Interwiki> interwikiList = mapper.reader().readValue(mapper.getFactory().createParser(interwikimapJSON), ref);
        for (Interwiki entry : interwikiList) {
            map.byPrefix.put(entry.prefix, entry);
        }
        return map;
    }
}
