package ipanema.links;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;

import static ipanema.links.Link.*;

/**
 * @see <a href="https://www.mediawiki.org/wiki/API:Siteinfo#Namespaces">API:Siteinfo#Namespaces</a>
 */
public class Namespace implements Serializable {
    private static final long serialVersionUID = 7177572193103397573L;

    /**
     * An integer identification number which is unique for each namespace.
     */
    int id;

    /**
     * The displayed name for the namespace.
     */
    String name;

    /**
     *  Provides the canonical namespace name of the namespace, which may or may not be the same as the actual
     *  namespace name. For example, all wikis have a "Project:" namespace, but the actual namespace name is normally
     *  changed to the name of the wiki (on Wikipedia for example, the "Project:" namespace is "Wikipedia:").
     *  @since 1.14
     */
    @JsonProperty
    String canonical;

    /**
     * Either "first-letter" or "case-sensitive". If the first letter of the namespace name is capitalized (the default),
     * then the value of this attribute will be "first-letter". Otherwise, the value will be "case-sensitive"
     * in order to indicate that the namespace name in question intentionally does not use a capital first letter.
     * @since 1.16
     */
    @JsonProperty("case")
    String _case;

    /**
     * The default <a href="https://www.mediawiki.org/wiki/Content_model">content model</a> for the namespace
     * @since 1.21
     */
    @JsonProperty
    String defaultcontentmodel;

    /**
     * Whether or not pages in this namespace can be transcluded.
     * @since 1.20
     */
    @JsonProperty
    boolean nonincludable;

    /**
     * This attribute indicates that pages within this namespace should be treated as the main content of the wiki.
     * Pages within namespaces with the content value set are counted for statistical purposes, among other things.
     *
     * @since 1.16
     */
    @JsonProperty
    boolean content;

    /**
     *  This attribute indicates that <a href="https://www.mediawiki.org/wiki/Help:Subpages">subpages</a> are
     *  allowed within the namespace.
     *
     *  @since 1.13
     */
    @JsonProperty
    boolean subpages;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCanonical() {
        return canonical;
    }

    public String getCase() {
        return _case;
    }

    public String getDefaultContentModel() {
        return defaultcontentmodel;
    }

    public boolean isContent() {
        return content;
    }

    public boolean areSubpagesAllowed() {
        return subpages;
    }

    public boolean isNonIncludable() {
        return nonincludable;
    }

    public boolean match(String term) {
        if (term == null) {
            return false;
        }
        final Matcher matcher = PREFIX.matcher(term);
        if (matcher.find() && term.length() > matcher.end()) {
            final String ns = unescapeSpace(matcher.group(1));
            return getCanonical().equalsIgnoreCase(ns) || getName().equalsIgnoreCase(ns);
        }
        return false;
    };

    @Override
    public String toString() {
        return "Namespace{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", canonical='" + canonical + '\'' +
            ", case='" + _case + '\'' +
            ", defaultcontentmodel='" + defaultcontentmodel + '\'' +
            ", nonincludable=" + nonincludable +
            ", content=" + content +
            ", subpages=" + subpages +
        '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Namespace namespace = (Namespace) o;
        return id == namespace.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
