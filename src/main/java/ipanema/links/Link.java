package ipanema.links;

import ipanema.language.model.Language;
import ipanema.language.model.LanguageData;
import org.apache.commons.text.StringEscapeUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ipanema.language.model.Identifier.Type.APPENDIX_CONSTRUCTED;
import static ipanema.language.model.Identifier.Type.RECONSTRUCTED;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;


public class Link {
    static final Pattern PREFIX = Pattern.compile("^:?([^:]+):");
    private static final Pattern FRAGMENT = Pattern.compile("^([^#]+)#(.+)$");
    private static final Pattern WIKILINKS = Pattern.compile("\\[\\[([^]]+)]]");
    private static final Pattern LINK_COMPONENTS = Pattern.compile("^([^|]+)\\|(.+)$");
    private static final Pattern URL_DECODED = Pattern.compile("%([0-9a-fA-F]{2})");
    private static final String SENSE_ID_SEPARATOR = ":_";
    private static final Namespaces NAMESPACES = Namespaces.load();
    private static final InterwikiMap INTERWIKIMAP = InterwikiMap.load();

    private static final Namespace APPENDIX = NAMESPACES.get("Appendix");
    private static final Namespace CITATIONS = NAMESPACES.get("Citations");
    private static final Namespace CATEGORY = NAMESPACES.get("Category");
    private static final Namespace RECONSTRUCTION = NAMESPACES.get("Reconstruction");

    private static final Set<String> IGNORE_CAP = new HashSet<>(singletonList("ko"));

    private final String target;
    private final Language language;
    private final String display;
    private final String fragment;
    private final String id;
    private final @Nullable Interwiki interwiki;
    private final @Nullable Namespace namespace;

    private static final LanguageData languageData = LanguageData.load();

    Link(String target, Language language, String display, String fragment, String id) {
        this.target = target;
        this.language = language;
        this.display = display;
        this.fragment = fragment;
        this.id = id;

        final Matcher prefixMatcher = PREFIX.matcher(target);

        if (prefixMatcher.find()) {
            final String prefix = prefixMatcher.group(1);

            this.namespace = NAMESPACES.get(prefix);
            this.interwiki = this.namespace == null ? INTERWIKIMAP.get(prefix) : null;
        } else {
            this.interwiki = null;
            this.namespace = null;
        }
    }

    public Link(String target, String language) {
        this(target,
             languageData.getLanguage(language).orElse(null),
             null, null, null);
    }

    public String getTarget() {
        return target;
    }

    public Language getLanguage() {
        return language;
    }

    public String getDisplay() {
        return display;
    }

    public String getFragment() {
        return fragment;
    }

    public String getId() { return id; }

    public @Nullable Interwiki getInterwiki() {
        return interwiki;
    }

    public @Nullable Namespace getNamespace() { return namespace; }

    public boolean isPrefixed() {
        return getNamespace() != null || getInterwiki() != null;
    }

    /**
     * @return A new link with the target part normalized, using the canonical namespace and interwiki links.
     *  Underscores are replaced with spaces, URL encoding is reversed and leading/trailing whitespace is trimmed.
     */
    public Link normalize() {
        final Matcher prefixMatch = PREFIX.matcher(target);
        if (prefixMatch.find()) {
            final String prefix = (getInterwiki() != null) ? getInterwiki().prefix :
                                  (getNamespace() != null) ? getNamespace().name : prefixMatch.group(1);

            final String page = target.substring(prefixMatch.end());
            final String newTarget = prefix + ":" + decodeURL(unescapeSpace(page, isPrefixed()));
            return new Link(newTarget, language, display, fragment, id);
        } else {
            final String newTarget = decodeURL(unescapeSpace(target));
            return new Link(newTarget, language, display, fragment, id);
        }
    }

    /**
     * @param term Text to turn into a link. This is generally the name of a page.
     * @param display The alternative display for the link, if different from the linked page.
     *                If this is null, the text argument is used instead
     * @param language The language object for the term being linked.
     * @param id Sense id string. If this argument is defined, the link will point to a language-specific sense id
     *           (identifier) created by the template {{senseid}}.
     * @return a list of links
     *
     * @see <a href="https://en.wiktionary.org/wiki/Module:links#language_link">Module:links#language_link</a>
     */
    public static List<Link> makeLinks(final @Nonnull String term,
                                       final @Nullable String display,
                                       final @Nonnull Language language,
                                       final @Nullable String id) {
        String text = term;
        if (IGNORE_CAP.contains(language.getCode())) {
            text = text.replaceFirst("\\^", "");
        }

        // Do we have embedded wikilinks?
        if (text.contains("[[")) {
            List<Link> links = new ArrayList<>();

            final boolean allReconstructed = text.startsWith("*");

            final Matcher matcher = WIKILINKS.matcher(text);
            while (matcher.find()) {
                final String link = matcher.group(1);
                String target = link, targetDisplay = null;

                final Matcher componentMatcher = LINK_COMPONENTS.matcher(link);
                if (componentMatcher.matches()) {
                    target = componentMatcher.group(1);
                    targetDisplay = componentMatcher.group(2);
                }

                if (allReconstructed) {
                    if (targetDisplay == null) {
                        targetDisplay = target;
                    }
                    target = "*" +  target;
                }
                links.add(makeLink(target, targetDisplay, language, null));
            }
            return links;
        } else {
            // There is no embedded wikilink, make a link using the parameters.
            return singletonList(makeLink(text, display, language, id));
        }
    }

    /**
     * Parse a MediaWiki URL into a link.
     * @param  uri a URI in the form of "./baa#English:_foo"
     */
    public static Link parse(URI uri) {
        String target;
        if (uri.getPath() != null) {
            target = removePath(deunderscore(uri.getPath()));
        } else {
            // should normally never happen
            target = uri.getScheme()+":"+uri.getSchemeSpecificPart();
        }

        final String fragment = uri.getFragment();
        String language = fragment;
        final String[] parts = getSenseId(fragment);
        String senseId = null;
        if (parts != null) {
            language = parts[0];
            senseId = "".equals(parts[1]) ? null : parts[1];
        }
        return new Link(
            target,
            language == null ? null : languageData.getLanguage(deunderscore(language)).orElse(null),
            null,
            fragment,
            senseId);
    }

    static @Nullable Link makeLink(final @Nonnull String term,
                                   final @Nullable String display,
                                   final @Nonnull Language language,
                                   final @Nullable String id) {
        String fragment = null;
        String displayText = display;

        // Find fragments
        // Prevents {{l|en|word#Etymology 2|word}} from linking to [[word#Etymology 2#English]].
        String target = decodeHTMLEntities(term);

        final Matcher matcher = FRAGMENT.matcher(target);
        if (matcher.matches()) {
            target = matcher.group(1);
            fragment = matcher.group(2);
        }

        // If there is no display form, then create a default one
        if (displayText == null) {
            displayText = target;

            // Strip the prefix from the displayed form
            if (displayText.startsWith(":")
                    && !LinksData.load().getUnsupportedTitles().contains(displayText)) {
                displayText = displayText.substring(1); // remove colon from beginning
            } else {
                final Matcher prefix = PREFIX.matcher(displayText);
                if (prefix.find() && INTERWIKIMAP.get(prefix.group(1)) != null) {
                    displayText = displayText.substring(prefix.end(1)+1); // remove prefix plus colon
                }
            }
        }
        target = getLinkPage(target, language);
        if (target == null) {
            return null;
        }

        // Add fragment
        final Matcher prefix = PREFIX.matcher(target);
        if (!prefix.find()
                || (INTERWIKIMAP.get(prefix.group(1)) == null)
                && !"Category".equals(prefix.group(1).trim())) {

            // Do not add a section link to "Undetermined", as such sections do not exist and are invalid.
            if (fragment == null && !language.getCode().equals("und")) {
                if (id != null) {
                    fragment = makeIdFragment(language, id);
                } else if (!APPENDIX.match(target) && !RECONSTRUCTION.match(target)) {
                    fragment = language.getCanonicalName();
                }
            }
            // This allows linking to pages like [[sms:a]] without it being treated weirdly.
            /*
            target = target.replace(":", "&#x3a;");
             */
        }
        return new Link(target, language, displayText, fragment, id);
    }

    static String unescapeSpace(String s) {
        return unescapeSpace(s, true);
    }

    static String unescapeSpace(String s, boolean trim) {
        return s == null ? null :
                (trim ? s.trim() : s)
                .replace('_', ' ');
    }

    static String decodeHTMLEntities(String text) {
        return StringEscapeUtils.unescapeHtml4(text);
    }

    static String decodeURL(String text) {
        if (text == null || !text.contains("%")) {
            return text;
        }
        final Matcher matcher = URL_DECODED.matcher(text);
        ByteArrayOutputStream bos = new ByteArrayOutputStream(text.length());
        int lastMatch = 0;
        while (matcher.find()) {
            final String substring = text.substring(lastMatch, matcher.start());
            try {
                bos.write(substring.getBytes(UTF_8.name()));
                bos.write(Integer.parseInt(matcher.group(1), 16));
            } catch (IOException | NumberFormatException ignored) {
            }
            lastMatch = matcher.end();
        }
        if (lastMatch == 0) {
            return text;
        }
        try {
            if (lastMatch < text.length()) {
                bos.write(text.substring(lastMatch).getBytes(UTF_8.name()));
            }
            return bos.toString(UTF_8.name());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String makeIdFragment(Language language, String id) {
        return language.getCanonicalName() + SENSE_ID_SEPARATOR + id.replace(" ", "_");
    }

    @Override
    public String toString() {
        return "[[" + target + (fragment == null ? "" : ("#"+fragment)) + "|" + display + "]]";
    }

    /**
     * @param target the link target
     * @param language the language to link to
     * @return the modified link page or null
     *
     * @see <a href="https://en.wiktionary.org/wiki/Module:links>Module:links#getLinkPage</a>
     */
    static @Nullable String getLinkPage(String target, Language language) {
        LinksData linksData = LinksData.load();

        if (linksData.getUnsupportedTitles().contains(target)) {
            return linksData.geReplacementTitle(target);
        }

        // If the link contains unexpanded template parameters, then don't create a link.
        if (target.contains("{{{")) {
            return null;
        }

        if (target.startsWith(":") || target.startsWith("w:") || target.startsWith("wikipedia:")) {
            return target;
        }

        // Remove diacritics from the page name
        target = language.makeEntryName(target);

        if (target.startsWith("/")) {
            target = ":" + target;
        }
        // Link to appendix for reconstructed terms and terms in appendix-only languages
        else if (target.startsWith("*") && target.length() > 1) {
            if (language.getCode().equals("und")) {
                return null;
            }
            target = "Reconstruction:" + language.getCanonicalName() + "/"  + target.substring(1);
        } else if (language.getType() == RECONSTRUCTED) {
            throw new RuntimeException("The specified language " + language.getCanonicalName() +
                    " is unattested, while the given word is not marked with '*' to indicate that it is reconstructed");
        } else if (language.getType() == APPENDIX_CONSTRUCTED) {
            target = "Appendix:" + language.getCanonicalName() + "/" + target;
        }
        return target;
    }

    public boolean isCategory() { return CATEGORY.equals(namespace); }
    public boolean isAppendix() { return APPENDIX.equals(namespace); }
    public boolean isCitation() { return CITATIONS.equals(namespace); }

    private static String removePath(String word) {
        return word.startsWith("/") ? word.substring(1) :
               word.startsWith("./") ? word.substring(2) :
               word;
    }

    private static String deunderscore(String s) {
        return s.replace('_', ' ');
    }

    private static String[] getSenseId(String fragment) {
        if (fragment == null) {
            return null;
        }
        String[] parts = fragment.split(SENSE_ID_SEPARATOR, 2);
        if (parts.length == 2) {
            return parts;
        } else {
            return null;
        }
    }
}
