from models_test import ModelsTest
from ipanema.models import LanguageFamily


class FamilyTest(ModelsTest):
    def test_query_by_iso_code(self):
        result = LanguageFamily.query('roa')
        self.assertEqual('Romance', str(result))

    def test_parent_family(self):
        west_iberian = LanguageFamily.query('roa-ibe')
        self.assertEqual('Romance', str(west_iberian.parent_family))

    def test_query_by_canonical_name(self):
        result = LanguageFamily.query('Romance')
        self.assertEqual('Romance', str(result))

    def test_query_by_alias(self):
        result = LanguageFamily.query('Romanic')
        self.assertEqual('Romance', str(result))

    def test_query_by_wikidata_identifier(self):
        result = LanguageFamily.query('Q19814')
        self.assertEqual('Romance', str(result))

    def test_repr(self):
        result = LanguageFamily.query('Q19814')
        self.assertTrue(repr(result))


if __name__ == '__main__':
    from unittest import main

    main()
