import os.path
import unittest

from ipanema.models import database, DEFAULT_DATABASE


class ModelsTest(unittest.TestCase):
    def setUp(self):
        super(ModelsTest, self).setUp()
        self.assertTrue(os.path.isfile(DEFAULT_DATABASE))
        database.init(DEFAULT_DATABASE)

    def tearDown(self):
        super(ModelsTest, self).tearDown()
        database.close()
