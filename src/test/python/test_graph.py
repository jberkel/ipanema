#!/usr/bin/env python
from models_test import ModelsTest
from ipanema.graph import cytoscape, nodes_and_edges


class GraphTest(ModelsTest):

    def test_nodes_and_edges(self):
        nodes, edges = nodes_and_edges()
        self.assertGreater(len(nodes), 6200)
        self.assertGreater(len(edges), 6100)

    def test_cytoscape(self):
        result = cytoscape(*nodes_and_edges())
        self.assertGreater(len(result), 12800)


if __name__ == '__main__':
    from unittest import main

    main()
