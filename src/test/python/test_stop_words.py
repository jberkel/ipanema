from models_test import ModelsTest
from ipanema.models import StopWords


class StopWordTest(ModelsTest):
    def test_query_by_code(self):
        words = StopWords.query('de')
        self.assertEqual(620, len(words))

    def test_query_by_code_empty(self):
        words = StopWords.query('xxx')
        self.assertEqual(0, len(words))


if __name__ == '__main__':
    from unittest import main

    main()
