#!/usr/bin/env python
import unittest

from ipanema import query_language


class IpanemaTest(unittest.TestCase):
    def test_query_language(self):
        result = query_language('pt')
        self.assertEqual('Portuguese', str(result))
        self.assertEqual('pt', result.code)
        self.assertEqual('West Iberian', str(result.family))
        self.assertEqual('Q5146', result.wikidata_item)

    def test_repr(self):
        result = query_language('LL.')
        self.assertIsNotNone(result)
        self.assertTrue(repr(result))


if __name__ == '__main__':
    from unittest import main

    main()
