from models_test import ModelsTest
from ipanema.models import Language


class LanguageTest(ModelsTest):
    def test_query_language_by_iso_639_3(self):
        result = Language.query('pt')
        self.assertEqual('Portuguese', str(result))

    def test_query_language_by_canonical_name(self):
        result = Language.query('Portuguese')
        self.assertEqual('Portuguese', str(result))

    def test_query_language_by_wikidata_identifier(self):
        result = Language.query('Q5146')
        self.assertEqual('Portuguese', str(result))

    def test_query_language_by_alias(self):
        result = Language.query('Modern Portuguese')
        self.assertEqual('Portuguese', str(result))

    def test_query_not_found(self):
        result = Language.query('XXX')
        self.assertIsNone(result)

    def test_repr(self):
        result = Language.query('pt')
        self.assertTrue(repr(result))

    def test_etymology_language(self):
        de_CH = Language.query('de-CH')
        self.assertEqual('Switzerland German', de_CH.canonical_name)

    def test_etymology_language_parent(self):
        de_CH = Language.query('de-CH')
        self.assertEqual('de', str(de_CH.parent))

    def test_ancestor(self):
        self.assertEqual('Latin', str(Language.query('osp').ancestor))

    def test_proto_ancestor(self):
        self.assertEqual('Proto-Brythonic', str(Language.query('Old Breton').ancestor))

    def test_ancestor_loop(self):
        self.assertEqual('Latin', str(Language.query('fro').ancestor))

    def test_equality(self):
        self.assertEqual(Language.query('en'), Language.query('English'))

    def test_hash(self):
        self.assertEqual(hash(Language.query('en')), hash(Language.query('English')))


if __name__ == '__main__':
    from unittest import main

    main()
