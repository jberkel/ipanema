package ipanema.links;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LinksDataTest {
    private LinksData data;

    @BeforeEach
    void setUp() {
        data = LinksData.load();
    }

    @Test
    void testGetUnsupportedTitles() {
        assertThat(data.getUnsupportedTitles()).hasSize(6);
        assertThat(data.getUnsupportedTitles()).contains("&amp;");
    }

    @Test
    void testGetReplacementTitle() {
        assertThat(data.geReplacementTitle("&amp;")).isEqualTo("Unsupported titles/`amp`amp;");
    }

    @Test
    void testGetReplacementTitleForValidTitleReturnsNull() {
        assertThat(data.geReplacementTitle("foo")).isNull();
    }
}
