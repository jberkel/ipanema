package ipanema.links;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class NamespacesTest {
    private Namespaces subject;

    @BeforeEach
    void setUp() {
        subject = Namespaces.load();
    }

    @Test
    void testLoad() {
        assertThat(subject.size()).isEqualTo(46);
    }

    @Test
    void testGetNames() {
        assertThat(subject.getNames()).hasSize(49);
        assertThat(subject.getNames()).contains("Appendix", "Reconstruction", "Module");
    }

    @Test
    void testEqualityIsBasedOnId() {
        Namespace n1 = new Namespace();
        Namespace n2 = new Namespace();

        assertThat(n1).isEqualTo(n2);

        n1.id = 1;
        assertThat(n1).isNotEqualTo(n2);

        n2.id = 1;
        assertThat(n1).isEqualTo(n2);
    }

    @ParameterizedTest
    @CsvSource(value = {
        "|Namespace{id=0, name='', canonical='null', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=true, subpages=false}",
        "Module|Namespace{id=828, name='Module', canonical='Module', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=false, subpages=true}",
        "Module talk|Namespace{id=829, name='Module talk', canonical='Module talk', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=false, subpages=true}",
        "Module_talk|Namespace{id=829, name='Module talk', canonical='Module talk', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=false, subpages=true}",
        "Project|Namespace{id=4, name='Wiktionary', canonical='Project', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=false, subpages=true}",
        "Wiktionary|Namespace{id=4, name='Wiktionary', canonical='Project', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=false, subpages=true}",
        "  Module  |Namespace{id=828, name='Module', canonical='Module', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=false, subpages=true}",
        "  moDulE  |Namespace{id=828, name='Module', canonical='Module', case='case-sensitive', defaultcontentmodel='null', nonincludable=false, content=false, subpages=true}",
        "Gadget definition|Namespace{id=2302, name='Gadget definition', canonical='Gadget definition', case='case-sensitive', defaultcontentmodel='GadgetDefinition', nonincludable=false, content=false, subpages=false}",
        "Special|Namespace{id=-1, name='Special', canonical='Special', case='first-letter', defaultcontentmodel='null', nonincludable=false, content=false, subpages=false}",
    }, delimiter = '|')
    void testGet(String name, String expected) {
        final Namespace namespace = subject.get(name == null ? "" : name);
        assertThat(namespace).isNotNull();
        assertThat(namespace.toString()).isEqualTo(expected);
    }
}
