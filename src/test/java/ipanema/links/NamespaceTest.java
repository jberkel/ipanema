package ipanema.links;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class NamespaceTest {
    private Namespace subject;

    @Nested
    class Simple {
        @BeforeEach
        void setUp() {
            subject = new Namespace();
            subject.name = "Foo";
            subject.canonical = "Canonical";
        }

        @ParameterizedTest
        @CsvSource(value = {
            "Foo:b",
            "Foo:bar",
            ": Foo :bar",
            ":foo:bar",
            "Canonical:bar",
            ":Canonical:bar",
        })
        void testMatch(String term) {
            assertThat(subject.match(term)).isTrue();
        }

        @ParameterizedTest
        @CsvSource(value = {
            "Foo:",
            "Foo:  ",
            "Foo",
            ":Foo",
            "Canonical",
            "Bar",
        })
        void testNonMatch(String term) {
            assertThat(subject.match(term)).isFalse();
        }
    }

    @Nested
    class WithSpaces {
        @BeforeEach
        void setUp() {
            subject = new Namespace();
            subject.name = "Foo talk";
            subject.canonical = "Canonical talk";
        }

        @ParameterizedTest
        @CsvSource(value = {
            "Foo talk:bar",
            ":Foo talk:bar",
            ":Foo_talk:bar",
            ": Foo talk :bar",
            ":foo talk:bar",
            "  Canonical talk:bar",
            ":Canonical talk:bar",
        })
        void testMatch(String term) {
            assertThat(subject.match(term)).isTrue();
        }

        @ParameterizedTest
        @CsvSource(value = {
            "Foo",
            ":Foo",
            "Canonical",
            "Bar",
        })
        void testNonMatch(String term) {
            assertThat(subject.match(term)).isFalse();
        }
    }
}
