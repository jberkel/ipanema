package ipanema.links;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class InterwikiMapTest {
    @Test
    void testLoad() {
        final InterwikiMap map = InterwikiMap.load();
        assertThat(map.size()).isEqualTo(805);
    }

    @Nested
    class Get {
        private InterwikiMap map;

        @BeforeEach
        void setUp() {
            map = InterwikiMap.load();
        }

        @ParameterizedTest
        @CsvSource(value = {
            "wikt|Interwiki{prefix='wikt', url='https://en.wiktionary.org/wiki/$1', language='null', local=true, localinterwiki=true, protorel=false}",
            "  wikt  |Interwiki{prefix='wikt', url='https://en.wiktionary.org/wiki/$1', language='null', local=true, localinterwiki=true, protorel=false}",
            "WIKT|Interwiki{prefix='wikt', url='https://en.wiktionary.org/wiki/$1', language='null', local=true, localinterwiki=true, protorel=false}",
            "iarchive|Interwiki{prefix='iarchive', url='https://archive.org/details/$1', language='null', local=false, localinterwiki=false, protorel=true}",
            "fr|Interwiki{prefix='fr', url='https://fr.wiktionary.org/wiki/$1', language='français', local=true, localinterwiki=false, protorel=false}",
        }, delimiter = '|')
        void testGet(String prefix, String expected) {
            final Interwiki interwiki = map.get(prefix);
            assertThat(interwiki).isNotNull();
            assertThat(interwiki.toString()).isEqualTo(expected);
        }
    }

    @Test
    void testGetPrefixes() {
        final InterwikiMap map = InterwikiMap.load();
        final Set<String> prefixes = map.getPrefixes();

        assertThat(prefixes).hasSize(805);
        assertThat(prefixes).contains("w", "wikt", "wikipedia");
    }
}
