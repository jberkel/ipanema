package ipanema.links;

import ipanema.language.model.Language;
import ipanema.language.model.LanguageData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ipanema.language.model.Identifier.Type.APPENDIX_CONSTRUCTED;
import static ipanema.language.model.Identifier.Type.RECONSTRUCTED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class LinkTest {
    private Language en, und, reconstructed, appendixConstructed;

    @BeforeEach
    void setUp() {
        en = LanguageData.ENGLISH;
        und = new Language() {
            @Override public String getCode() {
                return "und";
            }
        };
        reconstructed = new Language() {
            @Override public String getCanonicalName() {
                return "A reconstructed language";
            }
            @Override public Type getType() {
                return RECONSTRUCTED;
            }
        };
        appendixConstructed = new Language() {
            @Override public String getCanonicalName() {
                return "A constructed language";
            }
            @Override public String getCode() {
                return "foo";
            }
            @Override public Type getType() {
                return APPENDIX_CONSTRUCTED;
            }
        };
    }

    @Nested
    class Normalize {
        @ParameterizedTest
        @CsvFileSource(resources = "/normalize.csv", numLinesToSkip = 1)
        void testNormalize(String term, String display, String languageCode, String id, String expected) {
            final Optional<Language> language = LanguageData.load().getLanguage(languageCode);
            assertThat(language).isPresent();

            final Link link = Link.makeLink(term, display, language.get(), id);
            assertThat(link).isNotNull();
            assertThat(link.normalize().toString()).isEqualTo(expected);
        }
    }

    @Nested
    class UriParsing {
        @ParameterizedTest
        @CsvFileSource(resources = "/parse_links.csv", numLinesToSkip = 1)
        void testParse(String url, String namespace, String target, String languageCode, String fragment, String id) {
            URI uri = URI.create(url);
            Link link = Link.parse(uri);
            assertThat(link.getTarget()).isEqualTo(target);
            assertThat(link.getDisplay()).isNull();
            if (languageCode == null) {
                assertThat(link.getLanguage()).isNull();
            } else {
                assertThat(link.getLanguage()).isNotNull();
                assertThat(link.getLanguage().getCode()).isEqualTo(languageCode);
            }
            assertThat(link.getFragment()).isEqualTo(fragment);
            assertThat(link.getId()).isEqualTo(id);
            String actualNamespace = link.getNamespace() == null ? null : link.getNamespace().name;
            assertThat(actualNamespace).isEqualTo(namespace);
        }
    }

    @Nested
    class GetInterWiki {
        @Test
        void testPresent() {
            final Link c2 = Link.makeLink("c2:foo", "foo", en, null);
            assertThat(c2).isNotNull();
            assertThat(c2.getInterwiki()).isNotNull();
            assertThat(c2.getInterwiki().getUrl()).isEqualTo("http://c2.com/cgi/wiki?$1");
        }

        @Test
        void testIgnoreWhitespace() {
            final Link c2 = Link.makeLink("  c2  :foo", "foo", en, null);
            assertThat(c2).isNotNull();
            assertThat(c2.getInterwiki()).isNotNull();
            assertThat(c2.getInterwiki().getUrl()).isEqualTo("http://c2.com/cgi/wiki?$1");
        }

        @Test
        void testColonPrefixed() {
            final Link c2 = Link.makeLink(":c2:foo", "foo", en, null);
            assertThat(c2).isNotNull();
            assertThat(c2.getInterwiki()).isNotNull();
            assertThat(c2.getInterwiki().getUrl()).isEqualTo("http://c2.com/cgi/wiki?$1");
        }
    }

    @Nested
    class GetNamespace {
        @Test
        void testPresent() {
            final Link module = Link.makeLink("Module:foo", "foo", en, null);
            assertThat(module).isNotNull();
            assertThat(module.getNamespace()).isNotNull();
            assertThat(module.getNamespace().getName()).isEqualTo("Module");
        }

        @Test
        void testPresentNamespacePriority() {
            // there is also a prefix wiktionary:
            final Link wiktionary = Link.makeLink("Wiktionary:International Phonetic Alphabet", "foo", en, null);
            assertThat(wiktionary).isNotNull();
            assertThat(wiktionary.getNamespace()).isNotNull();
            assertThat(wiktionary.getNamespace().getName()).isEqualTo("Wiktionary");
        }

        @Test
        void testIgnoreWhitespace() {
            final Link module = Link.makeLink("  Module   :foo", "foo", en, null);
            assertThat(module).isNotNull();
            assertThat(module.getNamespace()).isNotNull();
            assertThat(module.getNamespace().getName()).isEqualTo("Module");
        }

        @Test
        void testDefaultReturnsNull() {
            final Link foo = Link.makeLink("foo", "foo", en, null);
            assertThat(foo).isNotNull();
            assertThat(foo.getNamespace()).isNull();
        }

        @Test
        void testColonPrefixedReturnsNull() {
            final Link foo = Link.makeLink(":foo", "foo", en, null);
            assertThat(foo).isNotNull();
            assertThat(foo.getNamespace()).isNull();
        }

        @Test
        void testUnknownReturnsNull() {
            final Link foo = Link.makeLink("XXX:foo", "foo", en, null);
            assertThat(foo).isNotNull();
            assertThat(foo.getNamespace()).isNull();
            assertThat(foo.getTarget()).isEqualTo("XXX:foo");
        }
    }

    @Nested
    class LinkGeneration {
        @ParameterizedTest(name = "makeLink({0}, {1}, {2}, {3}) == {4}")
        @CsvFileSource(resources = "/make_link.csv", numLinesToSkip = 1)
        void testMakeLink(String term, String display, String languageCode, String id, String expected) {
            final Optional<Language> language = LanguageData.load().getLanguage(languageCode);
            assertThat(language).isPresent();

            final Link link = Link.makeLink(term, display, language.get(), id);

            if (expected == null) {
                assertThat(link).isNull();
            } else {
                assertThat(link).isNotNull();
                assertThat(link.toString()).isEqualTo(expected);
                assertThat(link.getId()).isEqualTo(id);
            }
        }

        @ParameterizedTest(name = "makeLinks({0}, {1}, {2}, {3}) == {4}")
        @CsvFileSource(resources = "/make_links.csv", numLinesToSkip = 1)
        void testMakeLinks(String term, String display, String languageCode, String id, String expected) {
            final Optional<Language> language = LanguageData.load().getLanguage(languageCode);
            assertThat(language).isPresent();

            final List<Link> links = Link.makeLinks(term, display, language.get(), id);

            assertThat(links.stream()
                .map(Link::toString)
                .collect(Collectors.joining(";"))
            ).isEqualTo(expected);
        }
    }

    @ParameterizedTest
    @CsvSource(value = {
        "fo&#246;,foö",
        "fo&ouml;,foö"
    })
    void testDecodeHTMLEntities(String input, String expected) {
        assertThat(Link.decodeHTMLEntities(input)).isEqualTo(expected);
    }

    @ParameterizedTest
    @CsvSource(value = {
        ",",
        "foo,foo",
        "%,%",
        "%%,%%",
        "%41,A",
        "n%C3%AD,ní",
        "n%c3%ad,ní",
        "n%C3%AD%,ní%"
    })
    void testDecodeURL(String input, String expected) {
        assertThat(Link.decodeURL(input)).isEqualTo(expected);
    }

    @Nested
    class GetLinkPage {
        @Test
        void testUnsupportedTitleIsRewritten() {
            assertThat(Link.getLinkPage(" ", en)).isEqualTo("Unsupported titles/Space");
        }

        @Test
        void testUnexpandedTemplateParametersReturnsNull() {
            assertThat(Link.getLinkPage("{{{foo}}}", en)).isNull();
        }

        @ParameterizedTest
        @CsvSource(value = {"w:Foo", ":Foo", "wikipedia:Foo"})
        void testWikipediaLinksAreUnchanged(String link) {
            assertThat(Link.getLinkPage(link, en)).isEqualTo(link);
        }

        @Test
        void testDiacriticsAreStripped() {
            assertThat(
                Link.getLinkPage("pōpulus", LanguageData.load().getLanguage("la").get())
            ).isEqualTo("populus");
        }

        @Test
        void testLinkWithSlash() {
            assertThat(Link.getLinkPage("/foo", en)).isEqualTo(":/foo");
        }

        @Test
        void testReconstructedLinksToReconstructionNamespace() {
            assertThat(Link.getLinkPage("*foo", en)).isEqualTo("Reconstruction:English/foo");
        }

        @Test
        void testReconstructedUndefinedReturnsNull() {
            assertThat(Link.getLinkPage("*foo", und)).isNull();
        }

        @Test
        void testReconstructedWithoutAsteriskThrowsException() {
            assertThatThrownBy(
                () -> Link.getLinkPage("foo", reconstructed)
            ).hasMessage("The specified language A reconstructed language is unattested, while the given word is not marked with '*' to indicate that it is reconstructed");
        }

        @Test
        void testAppendixConstructedLinksToAppendix() {
            assertThat(Link.getLinkPage("foo", appendixConstructed)).isEqualTo("Appendix:A constructed language/foo");
        }
    }
}
