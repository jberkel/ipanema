package ipanema.language.model;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LanguageTest {

    @Test
    void testEqualityAndHashCode() {
        final Language l1 = new Language();
        final Language l2 = new Language();

        assertThat(l1).isEqualTo(l2);

        l1.canonicalName = "foo";

        assertThat(l1).isNotEqualTo(l2);

        l2.canonicalName = "foo";

        assertThat(l1).isEqualTo(l2);
        assertThat(l1.hashCode()).isEqualTo(l2.hashCode());
    }
}
