package ipanema.language.model;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.Optional;

import static ipanema.language.model.Identifier.Type.APPENDIX_CONSTRUCTED;
import static ipanema.language.model.Identifier.Type.ETYMOLOGY_LANGUAGE;
import static ipanema.language.model.Identifier.Type.FAMILY;
import static ipanema.language.model.Identifier.Type.RECONSTRUCTED;
import static ipanema.language.model.Identifier.Type.REGULAR;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

class LanguageDataTest {
    private LanguageData subject;

    @BeforeEach
    void setUp() throws IOException {
        subject = LanguageData.fromJSON(
            getClass().getResourceAsStream("/language/languages.json"),
            getClass().getResourceAsStream("/language/families.json"),
            getClass().getResourceAsStream("/language/scripts.json")
        );
    }

    @Test
    void testReadFromJSON() {
       assertThat(subject.size()).isEqualTo(9);
    }

    @Test
    void testGetByCodeReturnsEntry() {
        assertThat(subject.get("pt")).isPresent();
    }

    @Test
    void testGetByAliasReturnsEntry() {
        assertThat(subject.get("Modern Portuguese")).isPresent();
    }

    @Test
    void testLanguageIsCorrectlyDeserialized() {
        Language pt = subject.getLanguage("pt").get();

        assertThat(pt.getType()).isEqualTo(REGULAR);
        assertThat(pt.getCanonicalName()).isEqualTo("Portuguese");
        assertThat(pt.getCode()).isEqualTo("pt");
        assertThat(pt.familyCode).isEqualTo("roa-ibe");
        assertThat(pt.getFamily().map(Family::getCanonicalName)).contains("West Iberian");
        assertThat(pt.getAncestors()).containsExactly("roa-opt");
        assertThat(pt.getWikidataItem()).isEqualTo("Q5146");
        assertThat(pt.getScripts()).containsExactly("Latn", "Brai");
        assertThat(pt.getAliases()).containsExactly("Modern Portuguese");
    }

    @Test
    void testFamilyCodeIsCorrectlyDeserialized() {
        Language pt = subject.getLanguage("qsb-grc").get();
        assertThat(pt.getType()).isEqualTo(ETYMOLOGY_LANGUAGE);
        assertThat(pt.familyCode).isEqualTo("qfa-sub");
    }

    @Test
    void testLanguageVarietiesAreDeserialized() {
        Language gv = subject.getLanguage("gv").get();
        assertThat(gv.getCanonicalName()).isEqualTo("Manx");
        assertThat(gv.varieties).hasSize(2);

        assertThat(gv.varieties.get(0).name).isEqualTo("Northern Manx");
        assertThat(gv.varieties.get(1).name).isEqualTo("Southern Manx");
        // Note: this is a fake variety added for testing
        assertThat(gv.varieties.get(1).aliases).isEqualTo(new String[] { "South Manx" });
    }

    @Test
    void testEntryNameDeserialization() {
        Language la = subject.getLanguage("la").get();
        assertThat(la.entryName).isNotNull();
        assertThat(la.entryName.getRemoveDiacritics()).isEqualTo("̄̆̈͡");
    }

    @Test
    void testStandardCharsDeserializationSimple() {
        Language la = subject.getLanguage("gv").get();
        assertThat(la.standardChars).isNotNull();
        assertThat(la.standardChars.defaultValue).isNotNull();
        assertThat(la.standardChars.defaultValue.length()).isEqualTo(58);
    }

    @Test
    void testStandardCharsDeserializationMultiple() {
        Language la = subject.getLanguage("la").get();
        assertThat(la.standardChars).isNotNull();
        assertThat(la.standardChars.defaultValue).isNull();
        assertThat(la.standardChars.entries).hasSize(2);
        assertThat(la.standardChars.entries.get("1")).hasSize(8);
        assertThat(la.standardChars.entries.get("Latn")).hasSize(44);
    }

    @Test
    void testGetByNamePrefersCanonicalName() {
        assertThat(
            LanguageData.load().get("Quechua").map(Identifier::getCode)
        ).contains("qu");
    }

    @Test
    void testEtymologyLanguageType() {
        assertThat(
            LanguageData.load().get("LL.").map(Identifier::getType)
        ).contains(ETYMOLOGY_LANGUAGE);
    }

    @Test
    void testReconstructedLanguageType() {
        assertThat(
            LanguageData.load().get("gem-pro").map(Identifier::getType)
        ).contains(RECONSTRUCTED);
    }

    @Test
    void testAppendixConstructedLanguageType() {
        assertThat(
            LanguageData.load().get("Lojban").map(Identifier::getType)
        ).contains(APPENDIX_CONSTRUCTED);
    }

    @Test
    void testFamilyIsCorrectlyDeserialized() {
        Family roa = subject.getFamily("roa").get();
        assertThat(roa.getType()).isEqualTo(FAMILY);
        assertThat(roa.getCode()).isEqualTo("roa");
        assertThat(roa.getCanonicalName()).isEqualTo("Romance");
        assertThat(roa.getParent().map(Identifier::getCode)).contains("itc");
        assertThat(roa.getFamily().map(Family::getCanonicalName)).contains("Italic");
        assertThat(roa.protoLanguageCode).isEqualTo("la");
        assertThat(roa.getProtoLanguage().map(Language::getCode)).contains("la");
        assertThat(roa.getWikidataItem()).isEqualTo("Q19814");
        assertThat(roa.getAliases()).isEqualTo(asList(
            "Romanic",
            "Latin",
            "Neolatin",
            "Neo-Latin"
        ));
    }

    @Test
    void testScriptIsCorrectlyDeserialized() {
        Script latin = subject.getScript("Latn").get();
        assertThat(latin.canonicalName).isEqualTo("Latin");
        assertThat(latin.capitalized).isTrue();
        assertThat(latin.translit).isFalse();
        assertThat(latin.systems).containsExactly("alphabet");
        assertThat(latin.varieties).containsOnly(
            new Variety("Rumi"),
            new Variety("Romaji"),
            new Variety("Rōmaji"),
            new Variety("Romaja")
        );
    }

    @Test
    void testGetNonEtymological() {
        assertThat(subject.get("LL.")
            .flatMap(Identifier::getNonEtymological)
            .map(Identifier::getCode)
        ).contains("la");

        assertThat(subject.get("Latin")
            .flatMap(Identifier::getNonEtymological)
            .map(Identifier::getCode)
        ).contains("la");

        assertThat(subject.get("itc")
            .flatMap(Identifier::getNonEtymological)
            .map(Identifier::getCode)
        ).contains("itc");

        assertThat(subject.get("qsb-grc")
            .flatMap(Identifier::getNonEtymological)
            .map(Identifier::getCode)
        ).contains("qfa-sub");
    }

    @DisplayName("Converts the given term into the form used in the names of entries.")
    @CsvFileSource(resources = "/make_entry_name.csv")
    @ParameterizedTest(name = "makeEntryName({0}, {1}) == {2}")
    void testMakeEntryName(String lang, String term, String expected) {
        assertThat(LanguageData.load().makeEntryName(lang, term)).isEqualTo(expected);
    }

    @ParameterizedTest
    @ValueSource(strings = {"en", "pt", "es", "de", "fr"})
    void testGetDefaultData(String language) {
        final LanguageData defaultLanguageData = LanguageData.load();
        assertThat(defaultLanguageData.size()).isGreaterThan(8000);
        assertThat(defaultLanguageData.get(language)).isPresent();
    }
}
