include Makefile.common

PYTHON = python
LANG_DB   = $(DATA_DIR)/languages.sqlite
CREATE_DB = src/main/python/create_language_db.py
PYTHON_EXTRA_INDEX := https://gitlab.com/api/v4/projects/22495606/packages/pypi

# data dependencies
LANG_DATA     = $(DATA_DIR)/lang_data.json
LANG_FAMILIES = $(DATA_DIR)/lang_families.json
SCRIPTS_DATA  = $(DATA_DIR)/scripts_data.json
STOPWORDS     = $(DATA_DIR)/stopwords-iso.json

$(LANG_DB): $(CREATE_DB) $(LANG_DATA) $(LANG_FAMILIES) $(SCRIPTS_DATA) $(STOPWORDS) | .requirements
	@rm -f $@
	$(PYTHON) $(CREATE_DB) $(LANG_DATA) $(LANG_FAMILIES) $(SCRIPTS_DATA) $(STOPWORDS) $@

$(LANG_DATA):
	$(MAKE) -j4 -f Makefile.lang-data $@

$(SCRIPTS_DATA):
	$(MAKE) -j4 -f Makefile.lang-data $@

$(LANG_FAMILIES):
	$(MAKE) -j4 -f Makefile.lang-data $@

# https://github.com/stopwords-iso/stopwords-iso
$(STOPWORDS):
	curl $(CURL_OPTS) \
		https://raw.githubusercontent.com/stopwords-iso/stopwords-iso/master/stopwords-iso.json > $@

.PHONY: clean
clean:
	@rm -f $(LANG_DB)
	$(MAKE) -f Makefile.lang-data clean

lint: | .requirements
	$(PYTHON) -m flake8 setup.py src

test: lint
	PYTHONPATH=src/main/python $(PYTHON) -m unittest discover src/test/python

.PHONY: dist
dist: test
	$(PYTHON) setup.py bdist_wheel

.PHONY: upload
upload: dist
	@twine upload \
		--repository upload \
		--repository-url $(PYTHON_EXTRA_INDEX) \
		--username $(PYTHON_EXTRA_INDEX_USER) \
		--password $(PYTHON_EXTRA_INDEX_PASSWORD) \
		dist/*.whl

.PHONY: .requirements
.requirements: requirements.txt
	@pip install -r $< \
	    --extra-index-url $(PYTHON_EXTRA_INDEX)simple \
	    --quiet
